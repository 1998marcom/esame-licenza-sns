# List of possible ideas to expand

- [ ] $\mathbb{Z}_3$ and $\mathbb{Z}_N$ simulations
- [ ] Test - on a toy model - fermions with local-ish anti-commuting operators
  (suqa > 1903 > [14-16] (especially [16]))
