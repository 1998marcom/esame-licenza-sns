The Markov Chain Monte Carlo methods are a class of algorithms designed to
efficiently sample elements from a probability distribution. 

In the context of
physics, these samples are usually (micro)states of a system and once
collected can be used to perform measures and obtain information about the
system considered. These Monte Carlo methods can be used to study both
classical and quantum systems. Unfortunately, when classical Monte Carlo methods
are applied to some quantum systems, the infamous ``sign problem'' arises,
leading to increasing errors that make large and low temperature simulation
results unreliable. This problem, particularly common in the case of fermionic
or frustrated systems, has been proven to be, in its most general form, an
NP-hard problem\cite{Troyer_2005}.

Nevertheless, as shown in chapter 1, the ``sign problem'' 
can be tackled in some cases with the aid of a
recently developed quantum computing algorithm that goes under the name of
``Quantum Metropolis Sampling''\cite{Temme_2011}. In particular, this algorithm can be
efficiently applied to most systems of physical interest\cite{Lloyd_1996}.

One of these is surely the Hubbard model, illustrated in chapter 2.
This simple model is of practical interest, having been useful
in the understanding of strongly correlated electron systems and serving as a
paradigmatic model of the field. Notwithstanding its simplicity, it is a
fermionic system that suffers from
the sign problem and a general analytic solution of the model is not known. Much
of its low temperature phase diagram remains controversial,
too\cite{Arovas_2022}.

A classical simulator of a quantum computer has then being written and programmed, as
described in chapter 3, to simulate a few-qubits quantum computer running the
Quantum Metropolis Sampling algorithm applied to small examples of the Hubbard model.
Besides the system energy, 3 other observables are measured: 
the average spin-squared, the density of electrons and the
average double occupancy of a lattice site.
These measures together could differentiate between the main speculated phases
of the low temperature Hubbard model.

In chapter 4 a description and evaluation 
of the main possible error sources is provided. These include
finite correlation and rethermalization times, the
digitalization of states and energy and the errors related to the trotterization
procedure used to simulate the operators.

Finally, depicted in chapter 5 are our results, compared to results known from
literature. While our findings are mostly compatible with current understanding of
the low-temperature phase diagram, some sharp differences imputable to the low
system sizes arise with regard to the spin measures.

In conclusion we showed that the Quantum Metropolis Sampling can be successfully 
applied to small cases of the Hubbard model on a quantum computer simulator.
Further directions of research may include investigating larger models or models
immersed within a magnetic field, as well as coupling the algorithm with 
error correction techniques in order to try running it on a real quantum computer.
