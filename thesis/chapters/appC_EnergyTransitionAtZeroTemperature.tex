
\chapter{Energy transition at zero temperature} \label{ch:EnergyTransitionAtZeroTemperature}

In this appendix we show how the digitalization of energy can, at extremely low
temperatures, lead to undesired energy transitions in the QMS update step. 

Let us suppose we are applying the QMS algorithm with an $l$-qubit energy
register to explore, at zero temperature $\beta\rightarrow\infty$, a system that
has a minimum hamiltonian eigenvalue equal to $\theta_0=0$, the next biggest eigenvalue
$\theta_1 \ll 2\pi/2^l$, and all other eigenvalues $\theta_j \gg 2\pi/2^l$, with
$j>1$.

Generally, as shown in appendix \ref{ch:QPEProbabilityBound},
the probability of measuring an energy $b$ for an eigenstate whose eigenvalue is
$\theta=2\pi\left(\frac a{2^l}+\delta\right)$ is:
\begin{equation}
	\left|c_{b,\theta}\right|^2	= \left|\frac1{2^l}\sum_{X=0}^{2^l-1}
		e^{2\pi iX(a-b)/2^l} e^{2\pi iX\delta}\right|^2
\end{equation}
In our scenario this translates into:
\begin{align}
	\left|c_{b,\theta_0}\right|^2 &= \begin{cases}
		1 &\text{if } b=0\\
		0 &\text{if } b\neq0
	\end{cases}\\
	\left|c_{b,\theta_1}\right|^2	
		&= \left|\frac1{2^l}\sum_{X=0}^{2^l-1} e^{-2\pi iXb/2^l}e^{iX\theta_1}\right|^2 
\end{align}
Defining $\xi=\frac{\theta_1}{2}$, we can explicitly calculate 
$\left|c_{0,\theta_1}\right|^2$:
\begin{align}
	\left|c_{0,\theta_1}\right|^2	
		&= \left|\frac1{2^l}\sum_{X=0}^{2^l-1} e^{2iX\xi}\right|^2 = \\
	&= \left|\frac1{2^l}\frac{1-e^{2i2^l\xi}}{1-e^{2i\xi}}\right|^2 =\\
		&= \frac1{2^l}\frac{\left|1-e^{2i2^l\xi}\right|^2}{\left|1-e^{2
		i\xi}\right|^2} = \\
		&= \frac{\sin^2(2^l\xi)}{2^l\sin^2(\xi)}
		\label{eq:xi_result}
\end{align}

We now want to investigate the QMS-induced relative occupation of the two eigenspaces
$V_0$ and $V_1$ characterised by the two eigenvalues $\theta_0$ and $\theta_1$. Let us suppose
that the two eigenvalues have respectively degeneracy $g_0$ and $g_1$.

A familiar way to explore the relative occupation of the two eigenspaces is
introducing an effective temperature $\beta_\text{eff}$, so that:
\begin{align}
	\frac{\#_0}{\#_1} &=
		\frac{g_0}{g_1}\exp[\beta_\text{eff}(\theta_1-\theta_0)] \\
	\beta_\text{eff} &= \frac{g_1}{g_0} \frac{\dd (\#_0/\#_1)}{\dd \theta_1} 
		\bigg|_{\theta_1=\theta_0}
	\label{eq:betaeff_def}
\end{align}

Let us introduce a further semplification. We restrain ourselves to the case in
which the $U$ matrix is chosen with equal probability to its adjoint one, 
so that the QMS algorithm satisfies the detailed balance condition:
\begin{equation}
	\frac{\#_0}{\#_1} = \frac{p_{1\rightarrow0}}{p_{0\rightarrow1}}
	\label{eq:detailed_balance_sharp}
\end{equation}
\begin{comment}
In the limit of $\theta_1\ll2\pi/2^l$ we approximate
$\left|c_{j,\theta_1}\right|^2 \approx 0$ if $1<j<2^l-1$. We also require that
$\left|c_{2^l-1,\theta_z}\right|^2 \approx 0$ for any $z\neq1$. 
\end{comment}
We can then calculate:
\begin{align}
	p_{1\rightarrow0} &= K \frac{g_0}{g_0+g_1}\\
	p_{0\rightarrow1} &= K \frac{g_1\left|c_{0,\theta_1}\right|^2}{g_0+g_1\left|c_{0,\theta_1}\right|^2}
\end{align}
Plugging these two equations into \ref{eq:detailed_balance_sharp} and using the
result \ref{eq:xi_result}:
\begin{align}
	\frac{\#_0}{\#_1} &= \frac{g_0}{g_0+g_1} \,
	\frac{g_0+g_1\left|c_{0,\theta_1}\right|^2}{g_1\left|c_{0,\theta_1}\right|^2}=\\
	&=\frac{g_0}{g_0+g_1}\,\frac{g_0+g_1\frac{\sin^2(2^l\xi)}{2^l\sin^2(\xi)}}{g_1\frac{\sin^2(2^l\xi)}{2^l\sin^2(\xi)}}=\\
	&=\frac{g_0}{g_0+g_1}\,\frac{g_0\frac{2^l\sin^2(\xi)}{\sin^2(2^l\xi)}+g_1}{g_1}
\end{align}
Expanding for $\xi\ll\frac{\pi}{2^l}$:
\begin{align}
	\frac{\#_0}{\#_1} &= \frac{g_0}{g_0+g_1}\,
	\frac{g_0\frac{2^{2l}\xi^2\left(1-\frac13\xi^2\right)}{2^{2l}\xi^2\left(1-\frac132^{2l}\xi^2\right)}+g_1}{g_1}
	= \\[0.2cm]
	&=
	\frac{g_0\left[g_0\left(1+\frac13(2^{2l}-1)\xi^2\right)+g_1\right]}{g_1(g_0+g_1)}
\end{align}
where we negleted the terms in $\xi^4$ and higher orders. 

We can then evaluate $\beta_\text{eff}$ using \ref{eq:betaeff_def}:
\begin{align}
	\beta_\text{eff} &\approx \frac{g_1}{2g_0} \frac{\dd (\#_0/\#_1)}{\dd \xi} 
		\bigg|_{\xi\ll\frac{\pi}{2^l}} = \\
		&= \frac{g_1}{2g_0} \frac{g_0^2}{3g_1(g_0+g_1)}(2^{2l}-1)2\xi = \\
		&= \frac{g_0(2^{2l}-1)}{3(g_0+g_1)}\xi
		\label{eq:betaeff_xi}
\end{align}


This shows that the effective temperature that describes the eigenspaces
occupancies in the limit $\beta\rightarrow\infty$ is finite and proportional to
the energy gap $\theta_1-\theta_0$. In particular, in our case we chose our
scaling function $g(H)$ as a linear function that maps
$E_\text{min}\rightarrow0$ and $E_\text{max}\rightarrow\frac{2^l-1}{2^l}2\pi$.
This means that, calling $E_1$ the eigenvalue of the first excited state, we
have:
\begin{equation}
\xi=\frac12(\theta_1-\theta_0) \approx \pi\frac{E_1-E_\text{min}}{E_\text{max}-E_\text{min}}
\end{equation}
Plugging this into the result \ref{eq:betaeff_xi}, using $l=6$ and guesstimating
that in the worst case scenarios for our
hamiltonian parameters, with $J=0.02E_0$, we have $E_\text{max}-E_\text{min}\sim5E_0$,
$E_1-E_\text{min}\sim0.02E_0$, we finally get $\beta_\text{eff}\sim10$.
To compare this with the $\beta$ value used in the simulation we should rescale
\begin{equation}
\beta_\text{eff}'=\beta_\text{eff}\frac{2\pi}{E_\text{max}-E_\text{min}}\sim10E_0^{-1}
\end{equation}
Ultimately, this analysis qualitatively points out the fact that low temperature simulations
are not reliably representative of the chosen $\beta$ whenever
$\beta_\text{eff}'$ is of the same order of magnitude of $\beta$. 
