

\chapter{Systematics of errors} \label{ch:errors}

The thermal averages evaluated using the Quantum Metropolis Sampling are not
exact, even when the algorithm is performed on an ideal quantum computer whose qubits
and gates are free from error. In fact, there are 4 main factors that generally
contribute to the errors of the measures acquired using the QMS
algorithm~\cite{Clemente_2020}:
\begin{enumerate}
	\item rethermalization,
	\item digitalization of energy,
	\item digitalization of the state,
	\item trotterization of the Hamiltonian.
\end{enumerate}

\section{Rethermalization}
Similarly to what happens in the classical Metropolis algorithm, the measures
taken on a given quantum system are generally not independent from one another.

Like for the classical case, this is usually related to the 
graduality of the evolution steps of the algorithm. 
But in the quantum case, short
``waiting'' times between measures that do not commute with the
Hamiltonian $H$ lead to a worse problem. 
Indeed, when measuring an hermitian observable $A$ such that $[A,H]\neq0$, the
system register is projected into a state that is not an eigenstate of the
hamiltonian, and is not a representative sample of the thermal distribution of
states. In order to let the system register thermalise, in the sense that the
state stored is representative of the boltzmann distribution, \textit{a few} steps of
the QMS algorithm are required. In particular, figures \ref{fig:reth_en_time}
and \ref{fig:reth_en_corr_time} show how energy behaves as a function of the
number of QMS steps performed.

\begin{figure}
	\centering
	\begin{subfigure}{\textwidth}
		\centering
		\includegraphics[width=\textwidth]{energy_vs_time/main04.png}
		\caption{$\quad g(\min(\{E_j\}))\rightarrow0,\quad
		g(\max(\{E_j\}))\rightarrow2^l-1, \quad l=6$}
		\label{fig:reth_en_time_a0}
	\end{subfigure}
	\begin{subfigure}{\textwidth}
		\centering
		\includegraphics[width=\textwidth]{energy_vs_time/main05.png}
		\caption{$\quad g(\min(\{E_j\}))\rightarrow a,\quad
		g(\max(\{E_j\}))\rightarrow2^l-1-a,\quad l=6, \quad a=11$}
		\label{fig:reth_en_time_a10}
	\end{subfigure}
	\caption{Energy vs time for different linear scaling functions
	$g(H)\rightarrow H_\text{eff}$}
	\label{fig:reth_en_time}
\end{figure}
\begin{figure}
	\centering
	\begin{subfigure}{\textwidth}
		\centering
		\includegraphics[width=\textwidth]{energycorr_vs_tau/main04.png}
		\caption{Whole plot for $\tau$ up to half of the simulation time}
	\end{subfigure}
	\begin{subfigure}{\textwidth}
		\centering
		\includegraphics[width=\textwidth]{energycorr_vs_tau/main04-t.png}
		\caption{Enlargement of the first part of the plot}
	\end{subfigure}
	\caption{Energy correlation vs time delta for a low-temperature simulation}
	\label{fig:reth_en_corr_time}
\end{figure}
\begin{comment}
\begin{figure}
	\centering
	\begin{subfigure}{0.48\textwidth}
		\centering
		\includegraphics[width=0.7\textwidth]{ch04/c_vs_time_therm_1.png}
		\caption{}
		\label{fig:c_vs_time_therm_1}
	\end{subfigure}
	\begin{subfigure}{0.48\textwidth}
		\centering
		\includegraphics[width=0.7\textwidth]{ch04/c_vs_time_therm_2.png}
		\caption{}
		\label{fig:c_vs_time_therm_2}
	\end{subfigure}
	\begin{subfigure}{0.48\textwidth}
		\centering
		\includegraphics[width=0.7\textwidth]{ch04/c_vs_time_therm_4.png}
		\caption{}
		\label{fig:c_vs_time_therm_4}
	\end{subfigure}
	\begin{subfigure}{0.48\textwidth}
		\centering
		\includegraphics[width=0.7\textwidth]{ch04/c_vs_time_therm_8.png}
		\caption{}
		\label{fig:c_vs_time_therm_8}
	\end{subfigure}
	\caption{Four example graphs of recorded double occupancy with different
	thermalization times}
	\label{fig:c_vs_time_therm}
\end{figure}
\end{comment}

Given the correlations observed in figure \ref{fig:reth_en_corr_time}, we
settled for a value of rethermalization steps of 15. After this many steps, the
energy temporal correlation is $-$ in absolute terms $-$ compatible with 0
within the margin of fluctuations for the measured correlation.

An example of the measurements
taken is displayed in
figures
 \ref{fig:reth_time_csn}
  and \ref{fig:js_thermo}
for different values of the rethermalization time.

Furthermore, we apply the bootstrap resampling
technique to properly estimate the uncertainties for the inevitable remaining
correlation present between measurements at different times.

\begin{figure}
	\centering
	\begin{subfigure}{\textwidth}
		\centering
		\includegraphics[width=\textwidth]{csn_vs_time/thermo1.png}
		\caption{Rethermalization time: 1 QMS step}
	\end{subfigure}
	\begin{subfigure}{\textwidth}
		\centering
		\includegraphics[width=\textwidth]{csn_vs_time/thermo4.png}
		\caption{Rethermalization time: 4 QMS steps}
	\end{subfigure}
	\begin{subfigure}{\textwidth}
		\centering
		\includegraphics[width=\textwidth]{csn_vs_time/thermo15.png}
		\caption{Rethermalization time: 15 QMS steps}
	\end{subfigure}
	\caption{Three recorded observables, explained in chapter \ref{ch:results},
	sampled using different rethermalization times, as a function of the elapsed
	simulation time.}
	\label{fig:reth_time_csn}
\end{figure}
\begin{figure}
	\centering
	\begin{subfigure}{\textwidth}
		\centering
		\includegraphics[width=\textwidth]{UW_thermo/muj_scan_UW_thermo_1/js_rich_00/son.png}
		\caption{Rethermalization time: 1 QMS step}
	\end{subfigure}
	\begin{subfigure}{\textwidth}
		\centering
		\includegraphics[width=\textwidth]{UW_thermo/muj_scan_UW_thermo_4/js_rich_00/son.png}
		\caption{Rethermalization time: 4 QMS steps}
	\end{subfigure}
	\begin{subfigure}{\textwidth}
		\centering
		\includegraphics[width=\textwidth]{muj_ls/muj_l6/js_rich_00/son.png}
		\caption{Rethermalization time: 15 QMS steps}
	\end{subfigure}
	\caption{Three recorded observables, explained in chapter \ref{ch:results},
	sampled using different rethermalization times $\tau_\text{reth}$, at $\mu=\frac U2$ and for
	different values of $J$. For each value of $J$ a simulation is run for 2000
	QMS steps, and the uncertainties come from the statistical error
	of the $2000/(3\times\tau_\text{reth})$ measures for each observable.}
	\label{fig:js_thermo}
\end{figure}

\section{Digitalization of energy} \label{sec:energy_digitalization}

In the Quantum Metropolis Sampling algorithm, energy has to be stored in 
a quantum register, composed of $l$ qubits. If the spectrum of the effective
Hamiltonian $H_\text{eff}=g(H)$ cannot be exactly mapped to an $l$-bit binary number,
one introduces a truncation error in the energies and thus a systematic one 
in the sampling. 

In our case, and especially at low temperatures, this source of 
error is the most important one.
In fact, even for the 2x2 Hubbard lattice, the presence of energy eigenstates
whose eigenvalues are incommensurable~\cite{Schumann_2001} with each other make this problem
unavoidable.

At all temperatures, the problem is manifested by the fact that a
measure on the $E$ register does not make the system state in register $S$
collapse into an energy eigenstate, thus producing a systematic error in the
sampling.

At low temperatures, the same problem also arises in another form: transitions between
energy eigenstates that should never happen in an ideal QMS implementation, can indeed
happen when using a finite number of qubits for the energy register.
Quantitatively, as shown in appendix \ref{ch:QPEProbabilityBound},
the probability of measuring an energy $b$ for an eigenstate whose eigenvalue is
$\theta=2\pi\left(\frac a{2^l}+\delta\right)$ is:
\begin{equation}
	\left|c_{b,\theta}\right|^2	= \left|\frac1{2^l}\sum_{X=0}^{2^l-1}
		e^{2\pi iX(a-b)/2^l} e^{2\pi iX\delta}\right|^2
\end{equation}
This results in some energy eigenstates yielding overlapping energy measures
with the QPE algorithm. This overlap allows transitions between different states
to happen without having measured any change in the value of energy.

In particular, the simplest case of two energy eigenstates, with a small energy
separation $\delta E$, is analytically
treated in appendix \ref{ch:EnergyTransitionAtZeroTemperature}. The key result
is that there is an effective temperature $\beta_\text{eff}$ that describes
the QMS-generated distribution and scales proportionally with $\delta E$. This
limit is clearly only valid as long as $\beta_\text{eff} \ll \beta$. In particular, with
our simulation parameters, $\beta_\text{eff}$ spans from the order of 10s to the
order of 100s $E_0^{-1}$. This singular factor was the driving reason that made us choose
an extremal value of $\beta=50$ as the lowest simulated temperature, having
chosen to use $l=6$ qubits for the energy register to keep simulation times on
the order of a few days. Some simulations results for different values of $l$
are also reported in figure \ref{fig:example_l}.

\begin{figure}
	\centering
	\vspace{-10pt}
	\begin{subfigure}{\textwidth}
		\centering
		\includegraphics[width=\textwidth]{muj_ls/muj_l4/js_rich_00/son.png}
		\caption{$l=4,\; a=3$}
	\end{subfigure}
	\begin{subfigure}{\textwidth}
		\centering
		\includegraphics[width=\textwidth]{muj_ls/muj_l5/js_rich_00/son.png}
		\caption{$l=5,\; a=6$}
	\end{subfigure}
	\begin{subfigure}{\textwidth}
		\centering
		\includegraphics[width=\textwidth]{muj_ls/muj_l6/js_rich_00/son.png}
		\caption{$l=6,\; a=11$}
	\end{subfigure}
	\caption{Simulation measurements of different observables, later described in
	chapter \ref{ch:results}, at $\mu=\frac U2$, for different choices of the
	number of qubits $l$ and appropriate values of the parameter $a$ that describes the scaling
	function $g(H)\rightarrow H_\text{eff}$ as expressed in equation
	\ref{eq:a_definition}. For each value of $J$ a simulation is performed for
	2000 QMS steps.
	}
	\label{fig:example_l}
\end{figure}

An intuitive graphical depiction of the fact that there are different states
that can yield the same energy measure is provided in figure 
\ref{fig:state_dispersion}, where the exactly-solvable Hubbard lattice 
with $U=0, \mu =-E_0, J=0$ is initialized in the first excited state 
by inserting a particle in the system, and the probability distribution of
measuring a given energy after a QPE is reported for different numbers of used qubits in
the energy register.

A further consideration that arises from the results presented in figure
\ref{fig:state_dispersion}, and with particular clarity from
pictures \ref{fig:state_dispersion/E_3_polished} and
\ref{fig:state_dispersion/E_4_polished}, is that states with an effective energy 
close to zero have a non-null probability of being recorded as having an effective energy
 $E_\text{eff}=-1$. But since we are dealing with effective energies modulo $2^l$, this means
recording an effective energy of $2^l-1$, and possibly using that ``new'' energy
level to have a transition to another state with an high effective energy that
would \textit{almost} never be sampled if following an ideal thermal
distribution. Practical examples of these transitions are present in the data
reported in figure \ref{fig:reth_en_time_a0}. 
A possible way of dealing with this issue is spending some effective
energy levels at the edges of the spectrum, so that no state has an effective
energy outside an inner region where the sampling occurs. In particular,
minimizing the probability of transitions $E_\text{min}\rightarrow E_\text{max}$
has an expected cost of $\sim2^{l-1}$ energy levels, equivalent to devoting 1 qubit of the
energy register to the task. In our case we hand-tuned the hyperparameter of the
``empty'' energy levels and settled for dedicating $2a=22$ energy
levels out of $64$, while keeping $g(H)$ as a linear function, so that:
\begin{equation}
		\quad g(\min(\{E_j\}))\rightarrow a,\quad
		g(\max(\{E_j\}))\rightarrow2^l-1-a
		\label{eq:a_definition}
\end{equation}
The results of a simulation with such tuning is offered in
figure \ref{fig:reth_en_time_a10}.

In light of the aforementioned issues, all our results will suffer from the
problem of having to deal with a reduced number of qubits in the energy
register. Especially our results at the lower temperatures, such as those at 
$\beta=50E_0^{-1}$, are to be intended as being
representative of a low temperature simulation, but not necessarily accurately
depictive of the chosen value of $\beta$.

Further steps in the direction of lowering the errors caused by the
digitalization of energy might include using a larger
register for storing the energy values, as well as using more ingenious encodings for
floating-point variables~\cite{Nguyen_2014, Gayathri_2021, Haner_2018}, 
similarly to what happens on classical
machines\footnote{
\setlength{\baselineskip}{14pt}
	As a paradigmatic example of classical floating point
arithmetic, one may refer to the IEEE 754 standard~\cite{Ieee_2019}.}, while developing
relevant algorithms that keep efficiency on-par with the more widespread quantum
integer arithmetic, used in the current QPE implementation.

\begin{figure}
	\centering
	\makebox[\textwidth][c]{
		\begin{subfigure}{.54\textwidth}
			\centering
			\includegraphics[width=0.9\textwidth]{images/state_dispersion/E_3_polished.png}
			\caption{}
			\label{fig:state_dispersion/E_3_polished}
		\end{subfigure}
		\begin{subfigure}{.54\textwidth}
			\centering
			\includegraphics[width=0.9\textwidth]{images/state_dispersion/E_4_polished.png}
			\caption{}
			\label{fig:state_dispersion/E_4_polished}
		\end{subfigure}
	}
	\makebox[\textwidth][c]{
		\begin{subfigure}{.54\textwidth}
			\centering
			\includegraphics[width=0.9\textwidth]{images/state_dispersion/E_5_polished.png}
			\caption{}
			\label{fig:state_dispersion/E_5_polished}
		\end{subfigure}
		\begin{subfigure}{.54\textwidth}
			\centering
			\includegraphics[width=0.9\textwidth]{images/state_dispersion/E_6_polished.png}
			\caption{}
			\label{fig:state_dispersion/E_6_polished}
		\end{subfigure}
	}
	\makebox[\textwidth][c]{
		\begin{subfigure}{.54\textwidth}
			\centering
			\includegraphics[width=0.9\textwidth]{images/state_dispersion/E_7_polished.png}
			\caption{}
			\label{fig:state_dispersion/E_7_polished}
		\end{subfigure}
		\begin{subfigure}{.54\textwidth}
			\centering
			\includegraphics[width=0.9\textwidth]{images/state_dispersion/E_8_polished.png}
			\caption{}
			\label{fig:state_dispersion/E_8_polished}
		\end{subfigure}
	}
	\caption{Probability of measuring a given energy with the QPE algorithm, for
	a state with energy 1 and different numbers of qubits used for the energy
	register, with a fixed energy range $[0,8]$.}
	\label{fig:state_dispersion}
\end{figure}

\section{Digitalization of states}
Another source of possible errors is the fact that the system variable $S$ 
that holds the state of the system is encoded in a limited number of qubits.
In particular, this problem is of concern for bosonic and field theories. In the
first case, the number of particle has not an \textit{a-priori} bound, meaning
that even in a finite lattice the possible number of configurations of the
grand-canonical ensemble is infinite. This problem can usually be solved by
truncating high-energy states which are exponentially suppressed at finite
temperatures\cite{Lawrence_2020}. In the case of field theories, the problem is
similar to the classical simulation of bosonic field theories, where the
infinite degrees of freedom are again truncated by means of a finite lattice
spacing and a suitable floating-point representation for the value of the field
at any given point\cite{Lawrence_2020}.

Fortunately, in the case of the Hubbard model, we are working with a lattice
fermionic theory, whose degrees of freedom are finite once fixed the size of the
lattice. Nevertheless, the number of required qubits for the system register is
still $2\left|\Lambda\right|$, which, combined with the need to reserve some
qubits for the energy register, limits our computational capabilities to, at
most, small two-dimensional lattices.

\section{Trotterization}
The last source of errors we will discuss comes from the effective
implementation of an unitary transformation $U$ applied to the system register. 
We are going to discuss the problem both in the case of a real quantum computer
and in the case a classical simulator.

While on a real quantum computer any unitary operator $U$ can be arbitrarily approximated in terms of
any universal gate set \cite{Bogna_2020}, the approximation procedure
requires explicitly knowing the unitary operator $U$, which is usually not the
case when applying an unitary evolution operator
$U=e^{iHt}$. In these scenarios, we mostly happen to know only the hamiltonian
operator $H$. Generally speaking, also, any Hamiltonian operator $H$ can be
decomposed in the sum of many terms as $H=\sum_j H_j$ such that all $H_j$
operate on a bidimensional space and $e^{iH_jt}$ is then analytically computable
and easily implementable in terms of a universal gate set.
The problem is that $e^{iHt}=e^{i\sum_jH_jt}\neq\prod_je^{iH_jt}$, because the
various $H_j$ do not commute between each other. Nevertheless, there are
analytical expressions that allow to approximate $e^{i\sum_jH_jt}$ as a product
of simpler exponentials that go under the name of \textit{Trotter-Suzuki
formulas}~\cite{Hatano_2005}. At their lowest order, these boil down to observing that for small
values of $\delta $ we can effectively expand:
\begin{equation}
	e^{i\sum_j H_j \delta } = 
		\prod_j e^{iH_j \delta } + \mathcal{O}\left(\delta ^2\right)
\end{equation}
Therefore we can define $\delta=\frac tM$, with $M\in\mathbb N$, so
that:
\begin{align}
	e^{iHt} &= \left[e^{iH\delta}\right]^M \\
		&= \left[\prod_je^{iH_j\delta}+\mathcal{O}(\delta^2)\right]^M \\
		&= \left[\prod_je^{iH_j\delta}\right]^M +\mathcal{O}(\delta) 
\end{align}
We can then implement the unitary evolution of the system register by applying
$M$ times all the small evolution steps $e^{iH_j\delta}$, and control the error
we make by increasing the value of $M$ arbitrarily, at the cost of a larger
computational cost. These procedure is often
referred to as \textit{trotterization} and by its means it is possible to implement
any unitary operator whose respective hermitian matrix (in the sense of Stone's theorem)
is known.

In the scenario of a classical simulator, while the procedure of dividing the
time interval in smaller steps is still useful, there is no need to decompose the
hamiltonian $H$ as a sum of simpler hermitian matrixes. In particular, our task
of applying the exponential of a sparse matrix to the system state vector is
solved efficiently~\cite{Higham_2008} by a combination of:
\begin{itemize}
	\item subdividing the time interval $t$ in steps of length $\delta$
such that $\delta\lVert H\rVert_\text{max}\sim1$,
	\item applying the Krylov-Arnoldi algorithm, with a
computational complexity cost of $\mathcal{O}(2^n)$, where $n$ is the number of
qubits in the system register.
\end{itemize}
One further consideration is that for small instances of the Hubbard model, up
to 2x2 lattices in our implementation, explicitly computing the unitary matrix by means
of the Taylor expansion is computationally feasible and allows for a consistent
speedup. This is due to the fact that the unitary evolution $e^{iH\delta}$ is
still a sparse matrix because, as mentioned in chapter 2, the time evolution
operator only relates states with the same number of spin ups $\sum_i
n_{i\uparrow}$ and the same number of spin downs $\sum_i n_{i\downarrow}$. More
precisely, using equation \ref{eq:max_hubbard_submatrixes}, 
we can give an upper bound to the number of non-zero 
entries of the time evolution operator:
\begin{equation}
	\#_\text{non-zeros} \leq 2^n \binom{|\Lambda|}{\lfloor \frac{|\Lambda|}{2} \rfloor}^2
\end{equation}
where, due to the dimensionality of the Hubbard model,
$\left|\Lambda\right|=2^{n-1}$.
