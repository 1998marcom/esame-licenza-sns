\chapter*{Conclusions}
\label{ch:conclusions}\markboth{Conclusions}{}
\addcontentsline{toc}{chapter}{\nameref{ch:conclusions}}

Summing up our results, we showed that the quantum Metropolis sampling
algorithm on a quantum computer can efficiently simulate fermionic systems even at low temperatures. 
In particular the phase diagram of the Hubbard model can be 
investigated for small lattice sizes using a classical simulation. 
Nevertheless this means dealing with severe computational limitations
that in turn bring to a low accuracy of the physical measurements. 

In fact, other classical algorithms, like the density matrix renormalization group
(DMRG) and the determinant quantum Monte Carlo (DQMC),
are currently the state-of-the-art for characterizing the properties of
many-fermions systems~\cite{Arovas_2022}. 
Other approaches have also been proposed in the quantum computing framework. For
example, in the case of zero temperature another notable algorithm designed to run on a
quantum computer is the variational quantum eigensolver (VQE)~\cite{Tilly_2022}, 
which requires less resources than the quantum Metropolis sampling
for finding the properties of the ground state.

Our findings in the simulations are mostly in agreement with current knowledge
of the zero temperature phase diagram of the Hubbard model. We qualitatively
matched all the phases, but for the Neel anti-ferromagnetic order. The
sources of the systematic errors that prevented the detection of the phase
are probably related to the discretization of the energy levels. In
particular, we analytically showed that, where different states have close
energies compared to the energy discretization step, the
simulations results cannot be considered reliable. 
This issue needs to be addressed with further investigations.

Other directions of research might include experimenting 
with slightly bigger systems or larger registers by dedicating
more computing resources to the task, even though there will be diminuishing
returns for higher invested resources, as typical for a quantum computer
simulation. In particular, it might be worth to explore larger sizes of the
energy register and further explore how the different mappings
$g(H)\rightarrow H_\text{eff}$ could be used for better exploring the physics of
the model. One interesting case is in fact that in which the low energy part of
the spectrum gets expanded so that it can suffer less from the issues stemming
from the digitalization of the energy.

On a similar line, there is the prospect of developing a Quantum Phase
Estimation algorithm that can work efficiently with energies encoded in a
floating point format. This would in turn allow the Quantum Metropolis Sampling
to be used proficiently and with a good resolution at all temperatures. It would
also remove the need to properly tune the mapping function $g(H)$ that regulates
the range of the energies explored by the Quantum Phase Estimation.

Finally, another prospect of reasearch might be trying to combine some error correction 
code with the QMS algorithm so as to try running it on a real quantum
computer, even with smaller systems. A basic quantum error correction code proposed by Shor 
uses 9 physical qubits for each logical one to correct single qubit errors. So,
in order to simulate the small 2x2 Hubbard model, we would need
$9\times(8+6+1)=135$ qubits, and perform on each of them various hundreds of two-qubits
operations to realise a trotterized evolution $e^{iHt}$, the most expensive
computation of a step of the QMS on a real quantum computer. This results in
tens of thousands of gates needed in order to perform a single step of the QMS.
By comparison, current best
quantum computers are capable of performing a total of thousands of single-gate operations
before losing coherence within the registers. But progress is currently being
made and the figures change rapidly, so it is possible that this opportunity
might open in the near future.
