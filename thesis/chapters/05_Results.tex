
\chapter{Numerical results} \label{ch:results}
The results that follow come from the simulation of the Hubbard model as
described by the already mentioned Hamiltonian:
\begin{equation}
	\hat{H} = -J \sum_{\langle i, j\rangle,\sigma} \left( \hat{c}^\dagger_{i,\sigma}
	\hat{c}_{j,\sigma} + \hat{c}^\dagger_{j,\sigma} \hat{c}_{i,\sigma}
	\right) + U \sum_i \hat{n}_{i\uparrow} \hat{n}_{i\downarrow}
	- \mu \sum_i (\hat{n}_{i\uparrow} + \hat{n}_{i\downarrow})
	\label{eq:hubbard_ham}
\end{equation}
The lattice considered $\Lambda$ is a small 2x2 square lattice in all of the
simulations performed, the energy register is made up of $l=6$ qubits and the
parameter $a$ of the scaling function\footnote{
\setlength{\baselineskip}{14pt}
	The parameter $a$, as defined in section~\ref{sec:energy_digitalization},
	describes the scaling function $g(H)\rightarrow H_\text{eff}$ in the sense
	that we build $g(H)$ in each simulation so as to satisfy $g(E_\text{min})\rightarrow a$ and
	$g(E_\text{max})\rightarrow2^l-1-a$, where $E_\text{min}$ and $E_\text{max}$
	are respectively the smallest and the biggest eigenvalues of $H$.
} is set to 11.
\noindent The observables chosen to describe the system are the following:
\begin{itemize}
	\item The energy of the system, which is a ``free'' observable from the
		Quantum Metropolis Sampling algorithm. 

		Labelled \texttt{energy} in the plots.
	\item The average double occupancy of a site: 
$\ev{n_{i\uparrow}n_{i\downarrow}}=\frac1N\sum_in_{i\uparrow}n_{i\downarrow}$.
		
		Labelled \texttt{correlation} in the plots.
	\item The square of the per-site z-spin: 
		$\ev{n_{i\uparrow}-n_{i\downarrow}}^2=[\frac1N\sum_i(n_{i\uparrow}-n_{i\downarrow})]^2$.
		
		Labelled \texttt{spin} in the plots.
	\item The per-site number of particles, with the zero of the observable set
		at the half-filling level: 
		$\ev{n_{i\uparrow}+n_{i\downarrow}}-1=\frac1N\sum_i(n_{i\uparrow}+n_{i\downarrow}-1)$.
		
		Labelled \texttt{number} in the plots.
\end{itemize}

%NEW COMMENT:
%Ops, the previous comment and plots are not of real physical interest 
%as I was watching a case with
%$\mu=-1$, while the interesting physics (see figure \ref{fig:arovas_2022_pd}) is
%around $\mu=0.5U$ (or $\bar\mu = 0$). So I'd better checkout from now on what I
%want to look for before actually starting to look at it.

\section{Ground state results}
We initially started our investigation from extremely low temperatures, to get
results that can be compared with what is the most investigated scenario. In
particular we expect a description of the system that closely resembles the
ground state predictions as depicted in figure \ref{fig:arovas_2022_pd_mu}.

\subsection{Half filling}
Our first simulations are focused on the sign problem-free case of half-filling
at $\mu=\frac{U}{2}$. The results of the simulations are reported in figure
\ref{fig:muj_10_js_00}% for a 2x2 Hubbard lattice
%and in figure \ref{fig:muj_02_js_00} for a 3x2 one%
. 
\begin{figure}
	\centering
	\begin{subfigure}{.9\textwidth}
		\centering
		\includegraphics[width=\textwidth]{muj_scan_10s/muj_scan_10/js_rich_00/kane.png}
		\caption{}
		\label{fig:muj_10_js_00_kane}
	\end{subfigure}
	\begin{subfigure}{.9\textwidth}
		\centering
		\includegraphics[width=\textwidth]{muj_scan_10s/muj_scan_10/js_rich_00/son.png}
		\caption{}
		\label{fig:muj_10_js_00_son}
	\end{subfigure}
	\caption{Recorded observables at $\mu=\frac U2$. For each value of $J$ we
	performed a simulation with length of $10^4$ QMS steps and a
	rethermalization time of 15 QMS steps. The uncertainties
	displayed come from the statistical errors associated with the
	$10^4/(3\times15)\approx222$ measurements.}
	\label{fig:muj_10_js_00}
\end{figure}
\begin{comment}
\begin{figure}
	\centering
	\begin{subfigure}{.9\textwidth}
		\centering
		\includegraphics[width=\textwidth]{muj_scan_10s/muj_scan_10/js_rich_\n/kane.png}
		\caption{}
		\label{fig:muj_10_js_\n_kane}
	\end{subfigure}
	\begin{subfigure}{.9\textwidth}
		\centering
		\includegraphics[width=\textwidth]{muj_scan_10s/muj_scan_10/js_rich_\n/son.png}
		\caption{}
		\label{fig:muj_10_js_\n_son}
	\end{subfigure}
	\caption{}
	\label{fig:muj_10_js_\n}
\end{figure}
\end{comment}

The energy of the system
exhibits a strong dependence on $J$ for higher values of the parameter, matching
the results obtained for the 2x2 model by Schumann~\cite{Schumann_2001} and reported in
table \ref{tab:arovas_2022_hubbardsquare_gs}. 

%TODO: Must add two lines to \ref{fig:muj_02_js_00_kane} showing the results of
%Schumann\_2001 and tab:arovas\_2022\_hubbardsquare\_gs.

As for the three other observables, in the limit of $U \ll J$, we
obtain that $S_z^2\rightarrow~0$, $\langle
n_\downarrow n_\uparrow \rangle \rightarrow \frac14$, while the half filling situation
induced by $\mu=\frac{U}{2}$ is confirmed as the \texttt{number} variable stays
at 0. These observables are compatible with the ground state descripted by
corollary \ref{cor:lieb} and with the exact ground state at half
filling~\cite{Schumann_2001}. Simulations performed with classical techniques
for larger lattice sizes~\cite{Loh_1990} agree with the results here obtained in
this limit. From an immediate physical perspective, it is also
reasonable that, at low temperatures, 
in the limit of $\beta U\rightarrow0$, the two ``layers'' of the Hubbard
model, namely the particles with spin $+\frac12$ and those with spin $-\frac12$,
behave almost independently of one another, so that the double occupancy
observable goes to $\left(\frac12\right)^2=\frac14$.

In the case of strong coupling $U \gg J$, we
find an half-filled ferromagnetic state where $(2S_z)^2\rightarrow\frac13$, $\langle
n_\downarrow n_\uparrow \rangle \rightarrow 0$.
This is in contrast with the ground state of the exact solution as
reported in table \ref{tab:arovas_2022_hubbardsquare_gs}, namely an
antiferromagnetic one compatible with the corollary \ref{cor:lieb}.
Simulations performed with classical techniques~\cite{Loh_1990} confirm that in
the limit of strong coupling the ground state is an antiferromagnetic one.
Possible sources of this systematic error could include the particular choice of the
scaling function combined with the reduced number of qubits for the energy
registers. We explored also different choices for the parameter
$a=\{8,10,10.5\}$ of the
scaling function $g(H)$, but the results were comparable with the ones already
obtained for $a=11$.
Another possible contributing factor is that, as shown in previous
works~\cite{White_1989}, the antiferromagnetic behaviour predicted by the
corollary to Lieb's theorem for this case is clearer as the size of the system
increases. In particular, as already noted, the size of the systems being
simulated here is much smaller than the ones currently tackled with
state-of-the-art classical algorithms~\cite{Arovas_2022}.

\subsection{Greater particle density}

We then move our focus to explore the region of the phase diagram characterized
by higher values of $\mu$.
Simulation results for two different $\mu$ values are reported in figures
\ref{fig:muj_10_js_03} and \ref{fig:muj_10_js_04}.

From these two sampled values of $\mu$ it emerges that the transition threshold
of $U/J$ at which the average double occupancy goes to zero increases as $\mu$
grows. This is intuitively reasonable as a greater values of $\mu$ implies, as
also shown by the \texttt{number} observable, an higher number of particles, and
the double occupancy can reach 0 only at half filling, so an higher value of $U$
is required to ``drive away'' the particles in excess of half-filling.

As for the \texttt{spin} observable, we see that the single transition at
$\mu=0$ seems to split itself in two. As a consequence, a new phase with
intermediate values of \texttt{spin} arises for intermediate values of $U/J$.
Furthermore, we expect that for some
values of $0.5< \mu< 1$, as we move slightly away from half-filling, the
strong coupling limit $U/J \gg 1$ should reflect the Nagaoka scenario of an 
half metallic ferromagnet. This is
is unfortunately described by the same values of \texttt{number}, \texttt{spin}
and \texttt{correlation} obtained in the limit $U \gg J$ at $\mu=0.5$. Hence, while
our results match the theoretical predictions, we cannot differentiate between
two possible phases.

As for the \texttt{number} observable, we find out that, compatibly with the qualitative
speculative phase diagrams of figure \ref{fig:arovas_2022_pd}, there is a region
of intermediate values of $U/J$ where the system is away from half-filling, but
in both limits $U/J\rightarrow0$ and $U/J\rightarrow\infty$ 
this fades away and the system is forced back to half filling.

\foreach \n in {03,04}{
\begin{figure}
	\centering
	\begin{subfigure}{.9\textwidth}
		\centering
		\includegraphics[width=\textwidth]{muj_scan_10s/muj_scan_10/js_rich_\n/kane.png}
		\caption{}
		\label{fig:muj_10_js_\n_kane}
	\end{subfigure}
	\begin{subfigure}{.9\textwidth}
		\centering
		\includegraphics[width=\textwidth]{muj_scan_10s/muj_scan_10/js_rich_\n/son.png}
		\caption{}
		\label{fig:muj_10_js_\n_son}
	\end{subfigure}
	\caption{Recorded observables at $\mu\neq\frac U2$. For each value of $J$ we
	performed a simulation with length of $10^4$ QMS steps and a
	rethermalization time of 15 QMS steps. The uncertainties
	displayed come from the statistical errors associated with the
	$10^4/(3\times15)\approx222$ measurements.}
	\label{fig:muj_10_js_\n}
\end{figure}
}

We then proceeded to systematically explore the region of $\mu>0.5$. With the
spirit of trying to grasp the qualitative aspects of the phase diagram, we adopt
a more convenient representation for the data obtained in the form of
colorplots, without explicitly reporting the uncertainties associated with our
measures. Still, a graphical hint of the
uncertainties is given by the magnitude of the fluctuations.

The results obtained are presented for different choosings of the values of
$J$, namely in a linear scale in figure \ref{fig:muj_02} and in an inverted log
scale in figure \ref{fig:muj_10}.

The qualitative idea that emerges from the graphs is the same of the previously
described one: for higher values of $U/J$ there is an half metallic ferromagnet,
for lower values there is a antiferromagnetic state and, as $\mu$ increases,
a new phase appears, with a probable triple point at $\mu\sim0.65E_0$ and
$U/J\sim5$.

\begin{figure}
	\centering
	\makebox[\textwidth][c]{
		\begin{subfigure}{.54\textwidth}
			\centering
			\includegraphics[width=\textwidth]{muj_scan_02_short/alli_energy.png}
			\caption{}
			\label{fig:muj_02_alli_energy}
		\end{subfigure}
		\begin{subfigure}{.54\textwidth}
			\centering
			\includegraphics[width=\textwidth]{muj_scan_02_short/alli_correlation.png}
			\caption{}
			\label{fig:muj_02_alli_correlation}
		\end{subfigure}
	}
	\makebox[\textwidth][c]{
		\begin{subfigure}{.54\textwidth}
			\centering
			\includegraphics[width=\textwidth]{muj_scan_02_short/alli_spin.png}
			\caption{}
			\label{fig:muj_02_alli_spin}
		\end{subfigure}
		\begin{subfigure}{.54\textwidth}
			\centering
			\includegraphics[width=\textwidth]{muj_scan_02_short/alli_number.png}
			\caption{}
			\label{fig:muj_02_alli_number}
		\end{subfigure}
	}
	\begin{subfigure}{\textwidth}
		\centering
		\includegraphics[width=\textwidth]{muj_scan_02/eriksen_csn.png}
		\caption{}
		\label{fig:muj_02_eriksen_csn}
	\end{subfigure}
	\caption{Recorded observables in the $\mu-J$ plane. For each pair of values 
	$\mu, J$ we performed a simulation with length of $10^4$ QMS steps and a
	rethermalization time of 15 QMS steps. The figure is then obtained through a
	linear interpolation between the recorded points, after which
	filled contours are drawn for regions with similar values~\cite{contourpy}. The
	black and white images of correlation, spin, number are then merged mapping
	each image in a different channel of an RGB image.}
	\label{fig:muj_02}
\end{figure}
\begin{figure}
	\centering
	\makebox[\textwidth][c]{
		\begin{subfigure}{.54\textwidth}
			\centering
			\includegraphics[width=\textwidth]{muj_scan_10s/muj_scan_10_short/alli_energy.png}
			\caption{}
			\label{fig:muj_10_alli_energy}
		\end{subfigure}
		\begin{subfigure}{.54\textwidth}
			\centering
			\includegraphics[width=\textwidth]{muj_scan_10s/muj_scan_10_short/alli_correlation.png}
			\caption{}
			\label{fig:muj_10_alli_correlation}
		\end{subfigure}
	}
	\makebox[\textwidth][c]{
		\begin{subfigure}{.54\textwidth}
			\centering
			\includegraphics[width=\textwidth]{muj_scan_10s/muj_scan_10_short/alli_spin.png}
			\caption{}
			\label{fig:muj_10_alli_spin}
		\end{subfigure}
		\begin{subfigure}{.54\textwidth}
			\centering
			\includegraphics[width=\textwidth]{muj_scan_10s/muj_scan_10_short/alli_number.png}
			\caption{}
			\label{fig:muj_10_alli_number}
		\end{subfigure}
	}
	\begin{subfigure}{\textwidth}
		\centering
		\includegraphics[width=\textwidth]{muj_scan_10s/muj_scan_10/eriksen_csn.png}
		\caption{}
		\label{fig:muj_10_eriksen_csn}
	\end{subfigure}
	\caption{Recorded observables in the $\mu-J$ plane. For each pair of values 
	$\mu, J$ we performed a simulation with length of $10^4$ QMS steps and a
	rethermalization time of 15 QMS steps. The figure is then obtained through a
	linear interpolation between the recorded points, after which
	filled contours are drawn for regions with similar values~\cite{contourpy}. The
	black and white images of correlation, spin, number are then merged mapping
	each image in a different channel of an RGB image.}
	\label{fig:muj_10}
\end{figure}

\section{Results at finite temperature}
Finally, we explore how the phase diagram changes as the temperature increases.
We have chosen 4 values of $\beta$ for which to run the simulations:
$4E_0^{-1}$, $2E_0^{-1}$, $1E_0^{-1}$, $0.5E_0^{-1}$. 
The results obtained are presented in the colorplots \ref{fig:muj_11} $-$
\ref{fig:muj_14}.

The higher temperatures cause a progressive change in the measured observable
values. In particular, the insulation properties present at zero temperature,
$\mu=0.5$ and $U/J\gg1$ weaken as the double occupancy grows away from zero
even in this scenario. At higher values of $\mu$, the entropically favoured
states at half-filling cause a lower occupancy, which in turn reflects itself
in a lower average double occupancy in the same regions of the phase diagram.
%\vspace{100pt}

Also the \texttt{spin} observable at higher temperatures has a lower measured
value, as the singlet state with \texttt{spin}=0 has a comparable energy with
the ferromagnetic one, so the
two states easily mix. As an example, even at the not-so-high temperature of
$\beta=4E_0^{-1}$ the highest recorded \texttt{spin} is of $0.258\pm0.028$, well
below the value of $\frac13$ for the fully ferromagnetic scenario.

\foreach \n in {11,12,13,14}{
\begin{figure}
	\centering
	\makebox[\textwidth][c]{
		\begin{subfigure}{.54\textwidth}
			\centering
			\includegraphics[width=\textwidth]{muj_scan_10s/muj_scan_\n_short/alli_energy.png}
			\caption{}
			\label{fig:muj_\n_alli_energy}
		\end{subfigure}
		\begin{subfigure}{.54\textwidth}
			\centering
			\includegraphics[width=\textwidth]{muj_scan_10s/muj_scan_\n_short/alli_correlation.png}
			\caption{}
			\label{fig:muj_\n_alli_correlation}
		\end{subfigure}
	}
	\makebox[\textwidth][c]{
		\begin{subfigure}{.54\textwidth}
			\centering
			\includegraphics[width=\textwidth]{muj_scan_10s/muj_scan_\n_short/alli_spin.png}
			\caption{}
			\label{fig:muj_\n_alli_spin}
		\end{subfigure}
		\begin{subfigure}{.54\textwidth}
			\centering
			\includegraphics[width=\textwidth]{muj_scan_10s/muj_scan_\n_short/alli_number.png}
			\caption{}
			\label{fig:muj_\n_alli_number}
		\end{subfigure}
	}
	\begin{subfigure}{\textwidth}
		\centering
		\includegraphics[width=\textwidth]{muj_scan_10s/muj_scan_\n/eriksen_csn.png}
		\caption{}
		\label{fig:muj_\n_eriksen_csn}
	\end{subfigure}
	\caption{Recorded observables in the $\mu-J$ plane. For each pair of values 
	$\mu, J$ we performed a simulation with length of $4\times10^3$ QMS steps and a
	rethermalization time of 15 QMS steps. The figure is then obtained through a
	linear interpolation between the recorded points, after which
	filled contours are drawn for regions with similar values~\cite{contourpy}. The
	black and white images of correlation, spin, number are then merged mapping
	each image in a different channel of an RGB image.}
	\label{fig:muj_\n}
\end{figure}
}


