
\chapter{The Quantum Metropolis Sampling algorithm} \label{ch:QMS}
\section{Classical Metropolis and the sign problem} \label{sec:classical_to_sp}

In order to frame with some formality the problem of estimating thermal averages
for quantum systems, 
let us start by recalling the definition of the partition function:
\begin{equation}Z = \Tr[e^{-\beta H}]\end{equation}
where $H$ is the Hamiltonian of the system, $\beta = \frac{1}{k_BT}$ is the
inverse temperature, $k_B$ the Boltzmann constant and $T$ the temperature. 

Given the partition function, we can write the thermal average of any observable 
$A$ as:

\begin{equation}
	\expval{A}_T = \frac1Z \Tr[e^{-\beta H} A]
\end{equation}

One possible way of evaluating this is expanding $e^{-\beta H}$ as its Taylor
series and inserting an orthonormal basis between each term, so that:
\begin{align}
	Z 			 &= \sum_{n=0}^{\infty} \frac{(-\beta)^n}{n!}\Tr[H^n]\\
				 &= \sum_{n=0}^{\infty} \frac{(-\beta)^n}{n!}
					\sum_{(x_0,...,x_{n-1})\in \, [e(\mathcal{H})]^n} 
					\mel{x_0}{H}{x_1} ... \mel{x_{n-1}}{H}{x_0} \\
				 &= \sum_{n=0}^{\infty} \, \sum_{x_0,...,x_{n-1}} 
					w(x_0, ..., x_{n-1}, x_0) \\
	\expval{A}_T &= \frac1Z \sum_{n=0}^{\infty} \frac{(-\beta)^n}{n!}\Tr[H^nA]\\
				 &= \frac1Z \sum_{n=0}^{\infty} \frac{(-\beta)^n}{n!}
					\sum_{(x_0,...,x_{n})\in \, [e(\mathcal{H})]^{n+1}} 
					\mel{x_0}{H}{x_1} ... \mel{x_{n-1}}{H}{x_n} \mel{x_n}{A}{x_0} \\
				 &= \frac1Z \sum_{n=0}^{\infty} \, \sum_{x_0,...,x_{n}} 
					w(x_0, ..., x_n) \mel{x_n}{A}{x_0}
\end{align}

\noindent
where $\mathcal{H}$ is the Hilbert space representative of the quantum system,
and $e(\mathcal{H})$ is a generic orthonormal basis over this Hilbert space. We
have also defined the family of functions 
\begin{equation}
	w(x_0, ..., x_n) := 
		\frac{(-\beta)^n}{n!} \mel{x_0}{H}{x_1} ... \mel{x_{n-1}}{H}{x_n}
	\label{eq:weights_definition}
\end{equation}

Using $c=(n, (x_0, ..., x_{n}))$ as a shorthand for any possible configuration 
of the system, we can conveniently rewrite:
\begin{align}
	Z 			 &= \sum_c w(c) \\
	\expval{A}_T &= \frac1Z \sum_c w(c) A(c)
\end{align}

Assuming that all the
weights $w(c)$ are non-negative, we could use a Markov
Chain Monte Carlo algorithm, like the Metropolis one, using $w(c)$ as
as the weights of the possible configurations.

%For the purpose of digitally representing such configurations, it's useful to
%remark that, even though there is not an upper bound for $n$, configurations
%with higher values of $n$ are exponentially suppressed by the $n!$ term in the
%denominator in eq. \ref{eq:weights_definition}.

%EXPAND ON HOW TO BUILD A METROPOLIS GIVEN THIS FRAMEWORK (or give an example)

While the assumption that $w \geq 0 $ holds for bosonic systems and
non-frustrated spin systems, it does not hold for generic fermion systems with
two or more spatial dimensions, where the antisymmetric wave function gives rise
to configurations $c$ such that $w(c) < 0$. This is the core of
the fermion sign problem.

A common workaround is treating a negative value of $w(c)$ as representative
of a bosonic configuration with weight $|w|$, and ``transferring'' the sign to the
observable $A(c) \rightarrow -A(c)$. Still, this gives rise to errors that grow
exponentially with the inverse temperature $\beta$ and the particle number $N$.
As an example, it can be shown\cite{Troyer_2005} that the sign function of the weight 
$s(c)\:=\text{sign}(w(c))$, when evaluated using this workaround, has a relative
error of order:
\begin{equation}
	\frac{\Delta s}{\expval{s}}\sim \frac{e^{\beta N \Delta f}}{\sqrt{M}}
\end{equation}
where $\Delta f$ is the difference of free energy density between the bosonic and the
fermionic system, and $M$ is the number of measures.

Other commonly used and state-of-the-art algorithms in the context of bosonic systems, 
like the PIMC (Path Integral Monte Carlo), share the fermion sign problem when
applied to fermionic systems\cite{Kieu_1994}.
% TODO ADD SOURCE

\begin{table}
	\centering
	\begin{tabular}{r|m{.7\textwidth}}
		CLASS & DESCRIPTION \\
		\hline \hline \\[-0.3cm]
		
		 P & Decision problems solvable in polynomial time on a classical
			 machine\\[0.6cm]
		 NP & Decision problems whose answers correctness, given the answer, is 
		 	checkable in polynomial time on a classical machine\\[0.9cm]
		 NP-hard & Every NP-hard problem is at least as hard as every problem in 
		 	NP, meaning that a classical polynomial solution to any NP-hard problem
			implies a classical polynomial solution to all NP problems \\[1.2cm]
		 BQP & Problems solvable in polynomial time on a quantum computer, and
			 the answer is correct with high probability \\[0.5cm]
		
	\end{tabular}
	\captionsetup{width=0.7\textwidth}
	\caption{
		Computational classes for decision problems that are related to 
		the sign problem~\cite{Arora_2007}.
	}
	\label{tab:computational_classes}
\end{table}

The fermion sign problem has been shown to be $-$ in its most general case $-$
an NP-hard problem\cite{Troyer_2005}. This means that a generic solution of
polynomial complexity to the sign problem would also be a solution of polynomial 
complexity for any problem in the NP class. Most notably, the task of breaking
cryptography most often lies within the NP class.
Should such solution to the sign problem exist in the framework of classical computing,
this would mean that P=NP. Likewise, should a generic solution exist 
by the means of quantum computing, that would mean that
BQP=NP\cite{Arora_2007}. 

\section{A quantum computing solution}
While current research suggest that a generic solution to the sign problem is
unlikely in the quantum computing framework and even more so in the classical
computing one\cite{Arora_2007}, this does not prevent particular solutions to
the problem.

Most notably, Feynman's 1982 conjecture that local (physical) 
hamiltonians can be efficiently simulated on quantum computers has been shown to
be correct\cite{Lloyd_1996}. A solution to many cases of fermion sign problem 
that builds upon this finding is the Quantum Metropolis Sampling
algorithm\cite{Temme_2011}. This algorithm is designed to work on quantum
computers. We now proceed to illustrate the building blocks of this algorithm.
We then show how to combine those pieces into the Quantum Metropolis Sampling
(QMS) algorithm. 

For the purpose of this work, the choice of endiannes falls to the big-endian 
one. Intuitively, this means that a register that stores a value $X$ is composed 
by $x_i$ bits such that $X=\sum_{i=1}^n 2^{n-i} x_i$.

\subsection{Building blocks of the QMS algorithm}
The first sub-algorithm we are showing here is the Quantum Fourier Transform
(QFT). Let's start by introducing the quantum fourier basis:
\begin{equation}
	\ket{\psi_X} = \sum_Y \frac{\exp{2\pi i XY/N}}{\sqrt{N}} \ket{Y}
	\label{eq:fourier_basis}
\end{equation}
Where $N=2^n$ is the dimension of the Hilbert space and $n$ is the number of
qubits involved. It can be shown that this is indeed an orthonormal
basis\cite{Bogna_2020}. The QFT operator is then defined as:
\begin{equation}
	U_{\text{QFT}} = \sum_X \ketbra{\psi_X}{X}
\end{equation}
We now rewrite $U_{\text{QFT}}$ using \ref{eq:fourier_basis} and expanding
$\ket{Y} = \ket{y_1}_1 \otimes ... \otimes \ket{y_n}_n$:
\begin{align}
	U_{\text{QFT}} &= \sum_{X,Y} \frac{\exp{2\pi i XY/N}}{\sqrt{N}} \ketbra{Y}{X}\\
		&= \sum_X \frac{1}{\sqrt{N}} \sum_{Y} 
			\exp(\frac{2\pi i X}{N} \sum_{l=1}^n 2^{n-l} y_l) \ketbra{Y}{X} \\
		&= \sum_X \frac{1}{\sqrt{N}} \sum_{y_l \in \{0,1\}} \prod_{l=1}^n
			\exp(\frac{2\pi i X}{2^l} y_l) 
			\ket{y_1}_1 \otimes ... \otimes \ket{y_n}_n  \;\bra{X}\\
		&= \sum_X \frac{1}{\sqrt{N}} \bigotimes_{l=1}^n \left(\sum_{y_l \in \{0,1\}} 
			\exp(\frac{2\pi i X}{2^l} y_l) \ket{y_l}\right) \bra{X}\\
		&= \sum_X \frac{1}{\sqrt{N}} \bigotimes_{l=1}^n \left( 
			\ket{0}_l + \exp(\frac{2\pi i X}{2^l}) \ket{1}_l\right) \bra{X}
			\label{eq:nice_qft_1}
\end{align}
Elaborating more the exponential yields:
\begin{align}
	\exp(\frac{2\pi i X}{2^l}) 
		&= \exp(\frac{2\pi i}{2^l} \sum_{m=1}^n 2^{n-m} x_m) \\
		&= \prod_{m=1}^n \exp(2\pi i 2^{n-m-l} x_m)\\
		&= \prod_{m=n-l+1}^n \exp(2\pi i 2^{n-m-l} x_m)
\end{align}
Substituting back into eq. \ref{eq:nice_qft_1}:
\begin{equation}
	U_{\text{QFT}} = \sum_X \frac{1}{\sqrt{N}} \bigotimes_{l=1}^n \left( 
		\ket{0}_l + \prod_{m=n-l+1}^n \exp(2\pi i 2^{n-m-l} x_m) \ket{1}_l\right) \bra{X}
	\label{eq:nice_qft_2}
\end{equation}
The last expression \ref{eq:nice_qft_2} points out to the fact that the $l$-th qubit of the output
depends only on the last $l$ qubits of the input. It then makes sense to start
building a circuit that sequentially sets the qubits of the output, starting
with $\ket{y_n}_1$ the $n$-th qubit of the output 
in place of the $1^\text{st}$ qubit of the input, since
the first qubit of the input won't be needed for future calculations of qubits
$\ket{y_{n-1}}_2,...,\ket{y_1}_n$. Then we can use $\mathcal{O}(n^2)$ SWAP gates
to sort the qubits of the output as we wish.

From \ref{eq:nice_qft_2} it follows that the expression for $\ket{y_l}_{n-l+1}$ is:
\begin{equation}
	\ket{y_l}_{n-l+1} =
		\ket{0}_{n-l+1} + 
		\prod_{m=n-l+1}^n \exp(2\pi i 2^{n-m-l} x_m) \ket{1}_{n-l+1}
	\label{eq:y_l_nl1}
\end{equation}

A convenient way to build \ref{eq:y_l_nl1} is combining an Hadamard gate $H$
and multiple controlled phase shift gates $R_l$, as shown in figure 
\ref{fig:qft_yl}, where $R_l$ is defined as:
\begin{equation}
 R_l = 
	\begin{pmatrix}
		1 & 0 \\
		0 & \exp(\frac{2\pi i}{2^l})
	\end{pmatrix}
\end{equation}

\begin{figure}
	\centering
	\begin{subfigure}{0.75\textwidth}
		\centering
		\includegraphics[width=\textwidth]{ch01/qft_1.png}
		\caption{First step of QFT}
		\label{fig:qft_yl_1}
	\end{subfigure}
	\begin{subfigure}{0.74\textwidth}
		\centering
		\includegraphics[width=\textwidth]{ch01/qft_2.png}
		\caption{Second step of QFT}
		\label{fig:qft_yl_2}
	\end{subfigure}
	\caption{First steps of the QFT algorithm. Images taken from \cite{Bogna_2020}}
	\label{fig:qft_yl}
\end{figure}

Combining these steps sequentially and properly sorting the output qubits results
in the implementation of the whole $U_\text{QFT}$. As each step requires
$\mathcal{O}(n)$ gates and there are $\mathcal{O}(n)$ steps in this breakdown of
QFT, and sorting the qubits requires at most $\mathcal{O}(n)$ SWAP gates, 
the total number of used quantum gates is indeed $\mathcal{O}(n^2)$. 
This concludes this brief recap of the QFT algorithm.

\vspace{20pt}

The next important sub-algorithm for the Quantum Metropolis Sampling is the
Quantum Phase Estimation (QPE). This algorithm applies to the following problem.

Suppose being given a unitary operator $U$ with an eigenvector $\ket{u}$ whose
respective eigenvalue is $e^{i\theta}$, for some unknown $\theta \in [0, 2\pi)$. 
Assume being able to prepare the
$\ket{u}$ state and being able to perform controlled-$U^{2^j}$ gates, for $j \in
\mathbb{N}$. The problem is finding the best $l$-bit estimate for
$\frac{\theta}{2\pi}$.

The QPE solution is built on two quantum registers, one register $S$ containing 
the state $\ket{u}$ and the other one, $E$, made by $l$ qubits and used 
to give the result of the estimate procedure. 
Initially the state is initialized as follows:
\begin{equation}
	\ket{u}_S \otimes (\ket{+}_{E_1} \otimes ... \otimes \ket{+}_{E_l})
\end{equation}

Then, each qubit of the $E$ register is used to perform a controlled-$U^{2^j}$
operation on $\ket{u}_S$:
\begin{equation}
	\ket{u}_S \otimes \ket{+}_{E_j} \xrightarrow{\text{c-}U^{2^{l-j}}}
		\ket{u}_S \otimes \frac{\ket{0}_{E_j} + \exp(i2^{l-j}\theta)\ket{1}_{E_j}}{\sqrt{2}}
	\label{eq:qpe_cu}
\end{equation}
The resulting state can be written as:

\begin{align}
	\ket{\psi} &= \ket{u}_S \otimes \left[ \bigotimes_{j=1}^l \left(
			\frac{\ket{0}_{E_j} + \exp(i2^{l-j}\theta)\ket{1}_{E_j}}{\sqrt{2}}
		\right)\right] \\
		&= \frac1{\sqrt{2^l}} \ket{u}_S \otimes \left[ \bigotimes_{j=1}^l \left(
			\sum_{x_j\in\{0,1\}} \exp(i2^{l-j}\theta x_j)\ket{x_j}_{E_j}
		\right)\right] \\
		&= \frac1{\sqrt{2^l}} \ket{u}_S \otimes \left[ \sum_{X=0}^{2^l-1}
			\exp(i\theta X)\ket{X}_E
		\right]
	\label{eq:nice_qpe_1}
\end{align}

The final step of the QPE algorithm is an inverse QFT applied to the energy
register:
\begin{align}
	\ket{\psi} \xrightarrow{\text{QFT}^\dag \text{ on } E} \ket{\psi'}
		&=  \ket{u}_S \otimes U_\text{QFT}^\dag \left[ \frac1{\sqrt{2^l}}
			\sum_{X=0}^{2^l-1}	\exp(i\theta X)\ket{X}_E
			\right] \\
		&=  \ket{u}_S \otimes \sum_{Z,Y} \frac{\exp(-2\pi i ZY/2^l)}{\sqrt{2^l}} 
			\ket{Y}_E\bra{Z} \left[\sum_X \frac{e^{i\theta X}}{\sqrt{2^l}} \ket{X}_E
			\right] \\
		&=  \frac1{2^l}\ket{u}_S\otimes\sum_{X,Y}
			\exp[2\pi iX\left(\frac\theta{2\pi}-\frac Y{2^l}\right)]
			\ket{Y}_E
			\label{eq:final_qpe}
\end{align}
A measure in the computational basis is then performed on the energy register.
This measure yields the most significant $l$ bits of $\frac{\theta}{2\pi}$ 
with high probability. It is clear that, if $\theta \text{ mod } 2^{-l} = 0$ 
the algorithm works with probability 1. 

As shown in appendix \ref{ch:QPEProbabilityBound}, it also true that, for generic
values of $\theta$, the discrete $l$-bit value $Y$ closest to the real
$\theta$ is selected with probability greater than $\frac4{\pi^2}\sim 0.4$.
It has as well been shown that it is possible to increase this $l$-bit 
probability by increasing the number of qubits in the energy register.
More precisely, the best $l$-bit approximation of $\theta$ is obtained with
probability greater than $1-\varepsilon$ if the $E$ register contains 
$n = l + \mathcal{O}(\log(1/\varepsilon))$ qubits and the obtained result is 
rounded off to its most significant $l$ bits\cite{Benenti_2019, Cleve_1998}.

The Quantum Phase Estimation algorithm requires $\mathcal{O}(l)$ applications of
controlled $U^{2^j}$ and $\mathcal{O}(l^2)$ quantum gates for the inverse
quantum Fourier transform. This means that its efficiency is generally dependent
on the efficiency with which one can apply the control-$U$s. 

In the case of the
Quantum Metropolis Sampling, this $U$ matrix is just the exponential of the
Hamiltonian of the system, $U=e^{ig(H)}$, with $g$ a scaling function so that the
eigenvalues of $g(H)$ are in $[0, 2\pi)$. In other words, the Quantum Phase
Estimation used in the Quantum Metropolis Sampling is only efficient as long as
the temporal evolution of the system can be efficiently implemented in a quantum
computer circuitry. This, as already noted in section \ref{sec:classical_to_sp},
is indeed the case for most systems of physical interest.

It is also notable the fact that there is no need to initialise the $S$ register
into an eigenstate of the Hamiltonian. The algorithm can also be applied to 
superpositions of Hamiltonian eigenstates, and the final measure has also the
result of making the state in the $S$ register collapse into the relevant
Hamiltonian eigenstate.

\begin{figure}
	\centering
	\makebox[\textwidth]{
		\centering
		\Qcircuit @C=1.5em @R=.9em {
			\lstick{} & \multigate{1}{U^{2^2}} & \multigate{1}{U^{2^1}} &
				\multigate{1}{U^{2^0}} & \qw & \qw \\
			\lstick{} & \ghost{U^{2^0}} & \ghost{U^{2^1}} &
				\ghost{U^{2^2}} & \qw & \qw
			\inputgroupv{1}{2}{.8em}{1.0em}{\ket{u}_S}
				\\
			\lstick{\ket{+}_{E_1}} & \ctrl{-1} & \qw & \qw &
				\multigate{2}{\text{QFT}^\dag} & \qw\\
			\lstick{\ket{+}_{E_2}} & \qw & \ctrl{-2} & \qw & \ghost{\text{QFT}^\dag} & \qw\\
			\lstick{\ket{+}_{E_3}} & \qw & \qw & \ctrl{-3} & \ghost{\text{QFT}^\dag} & \qw
		}
	}
	\caption{Small scale QPE}
	\label{fig:qpe}
\end{figure}

\subsection{The ``Quantum Metropolis Sampling'' algorithm} \label{sec:QMS}
We now proceed to introduce the Quantum Metropolis Sampling (QMS) algorithm. The
idea behind the algorithm is sampling the Hilbert space with eigenstates
$\ket{u_j}$ of the Hamiltonian $H$ distributed accordingly 
to the Boltzmann distribution. The key point is that, while in the computational
basis the expectation value $\mel{i}{e^{-\beta H}}{i}$ yields $w(c)$ that can be
negative, the QMS operates in the basis $\{\ket{u_j}\}$ that diagonalises $H$,
and directly evaluates $\mel{u_j}{e^{-\beta H}}{u_j} > 0$.

We now describe a small variant of the algorithm as originally
proposed\cite{Temme_2011}. In particular, we make use of 3 quantum registers and
a classical one whereas \textit{Temme et al.} directly use 4 quantum registers.
This possibility of saving some resources stems from the fact that, in the
originial proposal, two of the four registers are never holding at the
same time a state that is not an element of the register's computational basis.

Our 3 quantum registers are labelled $S$, $E$, $a$, 
the classical one is labelled $E_\text{old}$:
\begin{itemize}
	\item The $S$ register must hold a representation of the state of the system whose
		thermal average we are interested in. 
	\item The $E$ register is an $l$-qubit multipurpose register used to take measures 
		of the system energy and potentially other observables. 
	\item The $a$ register is a single ancillary qubit used in the Metropolis to determine
		whether the evolution step should be accepted or not. 
	\item The $E_\text{old}$ register is an $l$-bit classical register used to store the
		previous result of the energy measurement.
\end{itemize}
Initially, the registers $S$ and $E_\text{old}$ are uninitialized, while $E$ and
$a$ are set to 0:
\begin{equation}
	\ket{\psi_0}_S \otimes \ket{0}_E \otimes \ket{0}_a , E_\text{old}=E_0
\end{equation}
where $\ket{\psi_0}_S$ is a generic state of the system register, that can also
be written in terms of the eigenstates of the Hamiltonian as 
$\ket{\psi_0}_S = \sum_j c_j \ket{u_j}_S$.

The algorithm then proceeds as follows:
\begin{enumerate}
	\item The registers $E$ and $a$ are set to 0. 
		The Quantum Phase Estimation algorithm $\Phi_\text{QPE}$ is applied to registers $S$ and
		$E$, using $U_\text{QPE}=e^{2\pi iH_\text{eff}/2^l}$ and an effective Hamiltonan 
			$H_\text{eff}=g(H)$\footnote{
\setlength{\baselineskip}{14pt}
			While we can be pretty generic with the function $g$, we still have to
			require that $H_\text{eff}$ has the same eigenvectors of $H$ and 
			that the respective action of the function on the eigenvalues is 
			invertible. %TODO Write this thing better. I mean E->E_eff must be invertible
			In this work the function has been chosen as a simple linear
			function: $g(H) = Ht-c$.
		}, so that $H_\text{eff}$ has a set of eigenvalues
		$\{E_{\text{eff},j}\}$ that satisfies 
		$0\leq E_{\text{eff},j}\leq2^l-1, $:
		\begin{equation}
			\sum_j c_j \ket{u_j}_S \otimes \ket{0}_E \otimes \ket{0}_a 
			\rightarrow
			\sum_j c_j \ket{u_j}_S \otimes \ket{E_{\text{eff},j}}_E \otimes \ket{0}_a
		\end{equation}
	\item A measure is performed on the energy register $E$, so that the state
		in the system register $S$ collapses into an eigenstate of the
		hamiltonian. The result of the measure is stored into the classical
		register $E_\text{old}$:
		\begin{equation}
			\sum_j c_j \ket{u_j}_S \otimes \ket{E_{\text{eff},j}}_E \otimes \ket{0}_a
			\rightarrow
			\ket{u_j}_S \otimes \ket{E_{\text{eff},j}}_E \otimes \ket{0}_a,
				E_\text{old}=E_{\text{eff},j}
		\end{equation}
	\item A random evolution matrix $U$ is chosen from a set of possible choices
		$\mathcal{C}$. The properties this set must satisfy are further 
		described in section \ref{sec:umatrix}. This operator $U$ is used to
		evolve the system register $S$:
		\begin{equation}
			\ket{u_j}_S \otimes \ket{E_{\text{eff},j}}_E \otimes \ket{0}_a
			\rightarrow
			\sum_k d_{kj} \ket{u_k}_S \otimes \ket{E_{\text{eff},j}}_E \otimes \ket{0}_a,
				E_\text{old}=E_{\text{eff},j}
		\end{equation}
	\item The energy register $E$ is reset to 0 and the 
		Quantum Phase Estimation algorithm $\Phi_\text{QPE}$ is applied to
		registers $S$ and $E$ in the same fashion as what done in step 1:
		\begin{equation}
			\sum_k d_{kj} \ket{u_k}_S \otimes \ket{E_{\text{eff},j}}_E \otimes \ket{0}_a
			\rightarrow
			\sum_k d_{kj} \ket{u_k}_S \otimes \ket{E_{\text{eff},k}}_E \otimes \ket{0}_a,
				E_\text{old}=E_{\text{eff},j}
		\end{equation}
	\item The ancillary qubit $a$ is updated with the quantum analogue of the
		accept-reject procedure. In particular, the operator $W(E_k, E_j)$ acts as
		follows:
		\begin{equation*}
			\sum_k d_{kj} \ket{u_k}_S \otimes \ket{E_{\text{eff},k}}_E \otimes \ket{0}_a,
				E_\text{old}=E_{\text{eff},j}
			\xrightarrow{W(E_k, E_j)}
		\end{equation*}
		\begin{equation}
			\sum_k d_{kj} \ket{u_k}_S \otimes \ket{E_{\text{eff},k}}_E \otimes 
				\left(\sqrt{f(E_{\text{eff},k}, E_{\text{eff},j})} \ket{1}_a+
				\sqrt{1-f(E_{\text{eff},k},E_{\text{eff},j})} \ket{0}_a\right)
		\end{equation}
		where
		\begin{equation}
			f(E_{\text{eff},k},E_{\text{eff},j})=
			\min\left(1,e^{-\beta(g^{-1}(E_{\text{eff},k})-g^{-1}(E_{\text{eff},j}))}\right)
		\end{equation}
	\item A measure of the ancillary qubit $a$ is performed. If the output of
		the measure is 1, the evolution step is accepted and the algorithm can
		continue from step 1. Othewise, we need to proceed to step 7 
		to revert the state of the
		system register $S$ to an eigenstate of eigenvalue $E_{\text{eff},j}$.
	\item 
		An efficient way\cite{Temme_2011} to try getting back to a state of
		energy equal to what stored in $E_\text{old}$ is applying the
		inverse of the operators we have applied:
		\begin{equation}
			V = W \circ \Phi_\text{QPE} \circ U \rightarrow 
			V^\dag = U^\dag \circ \Phi_\text{QPE}^\dag \circ W^\dag
		\end{equation}
		and then reset the energy register $E$ to 0, perform a $\Phi_\text{QPE}$
		using the registers $S$ and $E$ and measure the energy in the register
		$E$. Shall the result of this measure be equal to what stored in 
		$E_\text{old}$, our goal would be accomplished and we should start back
		from step 1.

		Otherwise we need to perform $\left[\Phi_\text{QPE}\right]^{-1}$,
		perform again steps $3-5$, take a measure of $a$ and $-$ independently
		of the result $-$ perform step 7
		again, until the loop exits because the measured energy from register
		$E$ is equal to the energy value stored in $E_\text{old}$.

\end{enumerate}

\noindent
It is worth noting that, unlike for the classical Metropolis algorithm, a reject of
the move doesn't mean the original state of the system is unaltered. Step 7
merely ensures that the new state has the same energy of the original state.
Generally speaking, the steps performed in the revert procedure can be
considered as a generic linear, trace-preserving, completely positive (LTCP) 
map\footnote{
\setlength{\baselineskip}{14pt}
Since there are measures that make the quantum computer system
interact with the environment, the evolution that ensues can generally be
described as the dynamic of an open system. And all open system dynamics are
representable with a linear, trace-preserving and completely positive (LCTP) map. 
Vice versa, each LCTP map can also be interpreted as an open system
dynamics~\cite{Bogna_2020}.
} that depends on the chosen
$U$. The set $\mathcal{C'}$ of these LTCP maps is then dependent on the set
$\mathcal{C}$.

\section{The \texorpdfstring{$U$}{U} matrix (and the
\texorpdfstring{$\mathcal{C}$}{C} group)}\label{sec:umatrix}
Two sufficient conditions for the set $\mathcal{C}$ as proposed in the original
paper that presented the algorithm\cite{Temme_2011} are:
\begin{enumerate}
	\item The set should ensure ergodicity inside the Hilbert space, so that
		there is a single fixed point. For example, this can easily be obtained 
		by choosing a universal gate set as $\mathcal{C}$.
	\item If an operator $U$ is in $\mathcal C$, then also $U^\dag$ must belong
		to $\mathcal C$. This implies a quantum 
		analogue of the detailed balance condition, which in turn ensures that
		thermal states are a stable set.
\end{enumerate}

When sampling only the energy, 
in this work we use a weaker condition to replace the first of the two.
Roughly speaking, the QMS algorithm is a Markov Chain whose possible $d$ states are 
associated with $d$ eigenstates of the Hamiltonian living in a $d$-dimensional
Hilbert space. We simply require the ergodicity of the QMS steps (that is, the
compound operation of applying $U$ and collapsing the state to an eigenstate of 
$H$) with respect to these possible set of $d$ states, but not the entire Hilbert
space. The ergodicity between the $d$ ``Markovian'' states 
is achievable by choosing $\mathcal{C} = \{U_0, U_0^\dag\}$, with
$U_0$ any unitary operator with no matrix entry equal to zero when expressed in basis of
the eigenvectors of the hamiltonian $H$. In particular, \textit{almost
any} matrix satisfies this property. In our work, we indeed choose to adopt such
a set, made of a random matrix $U_0$ and its adjoint one.

Working in this framework it is also easy to prove that the detailed balance
condition is satisfied for any thermal state $\rho = \sum_i \pi_i \ketbra{u_i}{u_i}$. 
In order to show this, let's start
by recalling the transition probabilities from the eigenstate $\ket{u_i}_S$ to
a generic eigenstate $\ket{u_k}_S$ when $U_0$ is applied and is expressed in the basis of
$\{\ket{u_j}\}_j$ by the coefficients $\{u_{ij}\}_{i,j}$:
\begin{equation}
	p_{i\rightarrow k} = \begin{cases}
		u_{ki}^*u_{ki} e^{-\beta\max(E_k-E_i,0)} 
		& \text{if } k \neq i \\
		
		u_{ii}^*u_{ii}+\sum_{k:E_k>E_i}u_{ki}^*u_{ki}\left[1-e^{-\beta(E_k-E_i)}\right]
		& \text{if } k = i
	\end{cases}
\end{equation}
%The global balance condition is then verified for the thermal state:
Then, supposing $E_k > E_i$, and applying $U_0$ with probability $\frac12$ and
$U_0^\dag$ with probability $\frac12$, we have:
\begin{align}
	p_{i\rightarrow k} &= \frac12\left(u_{ki}^*u_{ki} + u_{ik}u_{ik}^*\right)e^{-\beta (E_k-E_i)}\\
	p_{k\rightarrow i} &= \frac12\left(u_{ik}^*u_{ik} + u_{ki}u_{ki}^*\right)
\end{align}
The detailed balance condition easily follows for a thermal state
	$\pi_j\propto~e^{-\beta E_j}$:
\begin{equation}
	p_{i\rightarrow k} \pi_i = p_{k\rightarrow i} \pi_k
\end{equation}

When measuring observables other than the energy $H$, 
the ergodicity of the evolution with respect to the degenerate subspaces of $H$
can then be handled by re-extracting $U_0\in\text{U(N)}$ each time one such measures is taken.

% Moralmente sto riestraendo una nuova LTCP, il mio autovettore
% probabilisticamente unico della LTCP cambia e con 4 mazzi di conti verrà che
% per distribuzioni sensate sulle unitarie la LTCP viene estratta in modo
% sensato isotropo rispetto a qualunque autovettore di qualunque osservabile.
% È molto più facile verificare il bilancio dettagliato quantistico, ma non è
% quello che ho fatto io. Matematico che leggi questo commento su git nel 2042,
% hai un task: formalizza questo commento.


\begin{comment}
Then:
\begin{align*}
	\frac{\pi_k}{e^{-\beta E_k}}  
	&= \sum_i p_{i\rightarrow k} \pi_i e^{\beta E_k} 
	 = \sum_i p_{i\rightarrow k} e^{-\beta (E_i-E_k)} 
	 = \sum_ip_{i\rightarrow k}e^{-\beta (E_i-E_k)}\\
%   &= \sum_{i\neq k: E_i\leq E_k} p_{i\rightarrow k} e^{-\beta E_i} +
%		p_{k\rightarrow k} e^{-\beta E_k} +
%		\sum_{i: E_i > E_k} p_{i\rightarrow k} e^{-\beta E_i} \\
	&= \sum_{i\neq k: E_i\leq E_k} u_{ki}^*u_{ki} \;+ \\
	&\quad +
			u_{kk}^*u_{kk}+
			\sum_{i:E_i>E_k}u_{ik}^*u_{ik}\left[1-e^{-\beta(E_i-E_k)}\right] 
		\;+ \\
	&\quad + \sum_{i: E_i > E_k} u_{ki}^*u_{ki} e^{-\beta (E_i-E_k)} \\
	&= \sum_{i} u_{ki}^*u_{ki} +
		\sum_{i:E_i>E_k} 
		\left(u_{ik}^*u_{ik}-u_{ki}^*u_{ki}\right)
		\left[1-e^{-\beta(E_i-E_k)}\right] \\
	&= 1 +
		\sum_{i:E_i>E_k} 
		\left(u_{ik}^*u_{ik}-u_{ki}^*u_{ki}\right)
		\left[1-e^{-\beta(E_i-E_k)}\right] \\
\end{align*}
In a more compact writing:
\begin{align}
	\delta_{U_0} &= 
		\sum_{i:E_i>E_k} 
		\left(u_{ik}^*u_{ik}-u_{ki}^*u_{ki}\right)
		\left[1-e^{-\beta(E_i-E_k)}\right] \\
	\pi_k &= (1+\delta_{U_0})e^{-\beta E_k}
\end{align}


This means that, when performing a QMS update on a thermal state with the set of possible $U$
chosen as $\mathcal{C}=\{U_0,U_0^\dag\}$, we get:
\begin{equation}
	\pi_k = \left(\frac12(1+\delta_{U_0})+\frac12(1+\delta_{U_0^\dag})\right)e^{-\beta E_k}
		= e^{-\beta E_k}
\end{equation}
since, as easily verifiable, $\delta_{U_0} + \delta_{U_0^\dag} = 0$.
\end{comment}

\section{Local anticommuting operators}
In order to apply the Quantum Metropolis Sampling algorithm to a particular system we
must seek an efficient way to simulate the evolution operator $U=e^{iHt}$ on the
quantum, digital representation of such system. 

While on the one hand bosonic systems generally have issues with the fact that the unbounded
number of particles requires a potentially large number of qubits for their
representation, on the other hand simulating a second quantization
representation of a bosonic system is a pretty straightforward task. In fact,
local creation (or destruction) operators can be implemented as simply as the
operation of adding (or subtracting) 1 to the quantum register representing the
number of bosons in a given single-particle state. Such operation is sketched in
figure \ref{fig:subtract_1}. This implementation is local
(i.e. it does not require knowledge of the other registers) and the creation and
destruction operators of different registers 
do indeed satisfy the bosonic commutation relations:
\begin{equation}
	[a_i, a_j] = 0 \;,\; [a_i^\dag,a_j^\dag] = 0 \;,\; [a_i, a_j^\dag] = \delta_{ij}
	\label{eq:boson_commutation}
\end{equation}

\begin{figure}
	\centering
	\makebox[\textwidth]{
		\centering
		\Qcircuit @C=1.5em @R=.9em {
			\lstick{r_0} & \qw & \qw & \gate{\text{NOT}} & \qw \\
			\lstick{r_1} & \qw & \gate{\text{NOT}} & \ctrl{-1} & \qw \\
			\lstick{r_2} & \gate{\text{NOT}} & \ctrl{-1} & \ctrl{-1} & \qw 
		}
	}
	\captionsetup{width=0.5\linewidth}
	\caption{Circuit that subtracts 1 to a register, neglecting the	edge case
	$\ket{000}$}
	\label{fig:subtract_1}
\end{figure}

For what matters the simulation of fermionic degrees of freedom, the
implementation on a quantum computer architecture of creation and destruction
operators is not as straightforward and efficient at the same time due to the
desired anticommutation relations:
\begin{equation}
	\{a_i, a_j\} = 0 \;,\; \{a_i^\dag,a_j^\dag\} = 0 \;,\; \{a_i, a_j^\dag\} = \delta_{ij}
	\label{eq:fermion_commutation}
\end{equation}
An intuitive way to ensure these relations are satisfied is the Jordan-Wigner
transformation\cite{Mele_2020, Jordan_1928}, which relies on labelling the
fermion sites in any preestablished and consistent order, and then constructing
new fermionic $b_i$ and $b_i^\dag$ operators from the bosonic one-qubit respective
ones, $a_i$ and $a_i^\dag$:
\begin{gather}
	\eta_i = \bigotimes_{j<i} \left(\sum_{x\in\{0,1\}} (-1)^x
	\ket{x}_j\bra{x}\right) \\
	a_i = \ket{0}_i\bra{1} \;,\; a_i^\dag = \ket{1}_i\bra{0} \\
	b_i = \eta_i a_i \;,\; b_i^\dag=\eta_i a_i^\dag
\end{gather}
It is easy to show that the operators built do indeed satisfy the relations
\ref{eq:fermion_commutation}. 

Nevertheless, this solution has a severe efficiency penalty, as applying a single fermionic
operator requires also operating on the other fermionic registers. In
particular, all algorithms take a multiplicative factor $\mathcal{O}(n)$, where
$n$ is the number of possible fermionic single-particle states.

Newer algorithms have been
developed to amortise the cost of implementing anticommuting operators on quantum
registers to $\mathcal{O}(\log n)$ \cite{Bravyi_2002,Verstraete_2005} and
$\mathcal{O}(1)$ \cite{Whitfield_2016}

Finally, while this issue is of prominent importance in the case of algorithms
running on real quantum computers, it isn't when using a classical simulator of
a quantum computer. The simulated operators are just sparse matrices, and a
naive implementation of the Jordan-Wigner transformation merely yields some
sign changes to the respective bosonic matrices, thereby not increasing the
computational cost of simulating the system. In our work, we have relied on this
fact and have made use of the Jordan-Wigner transformation.
















