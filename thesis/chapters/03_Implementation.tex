
\chapter{Implementation}
In this chapter we propose a more technical and pratical in-depth
description of the simulation performed, with the help of some pseudo-code
snippets.
\section{Generalities}
Generally speaking, all the computation was performed using sparse matrix
algebra, resulting in a computational complexity cost of at most
$\mathcal{O}(2^n)$ operations, where $n$ was the total number of qubits used. Furthermore,
we wrote and made use of the \texttt{wtq} library \cite{repo-wtq} that helps to keep track
of the lack of entanglement between different qubits. 

These choices in turn allowed us to
implement the QMS algorithm as first published \cite{Temme_2011}, while 
avoiding unnecessarily keeping track of the two energy registers at the same
time, hence reducing the computational cost by $\mathcal{O}(2^l)$ to the
cost of simulating the QMS as described in chapter \ref{ch:QMS}.

In a similar way to what done in our classical simulation, we remark that it is
possible to perform the QMS on a quantum computer using only one energy
register, leading to a minor utilization of qubits.

\section{Simplified simulation program}
We now introduce step-by-step a pseudo code version of our simulation program.
\begin{enumerate}
	\item The quantum registers $S,E,a$ are declared with appropriate size, the
		effective hamiltonian $H_\text{eff}$ is computed, and the basic
		operators for the QPE, the inverse QPE, and the $W$ operator as
		described in step 5 in section \ref{sec:QMS} are instanciated.
\begin{minted}[bgcolor=codegray,tabsize=4,baselinestretch=1.1]{python}
S = QVariable(X*Y*2)
E = QVariable(l)
a = QVariable(1)

H = generate_hubbard_hamiltonian(X, Y, J, U, mu)
Heff = rescale_hamiltonian(H)

qpe = QPE(Heff)
inverse_qpe = QPE_inverse(Heff)
W = AcceptanceOperatorW(rescale_hamiltonian)
\end{minted}
	As a first note, we intend all operators here mentioned, \texttt{qpe},
		\texttt{qpe\_inverse} and \texttt{W}, as caring for the proper
		initialization of their required registers. If resetting a register is
		required, the procedure shall be done only after measuring said
		register. Otherwise, resetting a register without a prior meaure could
		lead to a non-pure state on the remaining registers. This in turn may
		``declass'' the other registers' states to being rappresented with the
		density matrix formalism, usually associated with a computational cost
		$\mathcal{O}(2^{2n})$.

		As a second note, we remark that, while it is satisfactory for
		\texttt{H}, \texttt{Heff} and \texttt{W} being explicitly stored as
		sparse matrixes, in most scenarios the operators \texttt{qpe} and
		\texttt{qpe\_inverse} are not explicitly calculated, but rather
		just-in-time applied using the trotterization techniques.

	\item The following steps constitute the loop of the simulation, to be
		performed \texttt{total\_steps} times.
\begin{minted}[bgcolor=codegray,tabsize=4,baselinestretch=1.1]{python}
FOR(j, total_steps)
\end{minted}

	\item Through the Quantum Phase Estimation operator previously instanced, we
		measure the initial energy of the system.
\begin{minted}[bgcolor=codegray,tabsize=4,baselinestretch=1.1]{python}
	# Eold
	qpe(S, E)
	Eold = E.measure()
\end{minted}

	\item After the system has been simulated for \texttt{thermalize\_steps}
		ticks, record a generic observable that doesn't commute with the
		hamiltonian, eventually with the help of the $E$ register or other
		ancillary registers.

		Then, since we are on a classical simulator, we use the QPE algorithm and
		then project (or force-measure) the energy register to record the same energy
		obtained before measuring the observable. While this step does not
		revert the state of the system to the initial state in case of energy
		degeneracy or a limited number of qubits in the energy register, it does
		bring the state of the system register \textit{closer} to the initial state.
\begin{minted}[bgcolor=codegray,tabsize=4,baselinestretch=1.1]{python}
	# Measurements
	if (j % thermalize_steps == 0)
		record_observable(S)
		# Classical superpowers to revert 
		# the system to energy Eold
		qpe(S, E)
		E.force_measure(Eold) 
\end{minted}

	\item Every \texttt{thermalize\_steps}, after the non-hamiltonian-commuting
		observable has been measured, re-extract the random $U$ matrix used for the
		QMS update step. At every step decide whether to use $U$ or $U^\dag$ with
		equal probability so as to satisfy the detailed balance condition.
\begin{minted}[bgcolor=codegray,tabsize=4,baselinestretch=1.1]{python}
	if (j % thermalize_steps == 0)
		base_random_U = get_random_unitary()

	if (random({0,1}) == 0)
		random_U = base_random_U
	else
		random_U = base_random_U.adjoint()
\end{minted}

	\item Then perform the evolution step as dictated by the QMS algorithm: use
		the random $U$ matrix on register $S$ to select a new state, perform the
		QPE on it, and compute with the operator $W$ the acceptance probability.
		If the update is accepted, there is nothing to do and the sampling can
		continue. On the contrary, if the update is rejected, the system state
		must be reverted to energy $E_\text{old}$. This is done applying the
		inverse QPE, the inverse of the $U$ matrix, similarly to what prescribed
		in the usual QMS algorithm, but then we make use of our classical
		superpowers to ensure that the energy we measure is the same
		$E_\text{old}$ we started with.
\begin{minted}[bgcolor=codegray,tabsize=4,baselinestretch=1.1]{python}
	# QMS
	S *= random_U
	qpe(S, E)
	W(E, Eold, a)
	if (a.measure() == 1)
		# Accept update - nothing to be done
		continue
	else
		# Reject update
		inverse_qpe(S, E)
		S *= random_U.adjoint()
		qpe(S, E)
		E.force_measure(Eold) # Classical superpower
\end{minted}
\end{enumerate}

\noindent
For the sake of completeness, we report also how we could replace the reject
path so as to reproduce exactly the step that should be performed on a quantum
computer:
\begin{minted}[bgcolor=codegray,tabsize=4,baselinestretch=1.1]{python}
# Proper reject procedure

# Same steps as above
inverse_qpe(S, E)
S *= random_U.adjoint()
qpe(S, E)

# But then no classical superpowers
while ( E.measure() != Eold )
	inverse_qpe(S, E)

	S *= random_U
	qpe(S, E)
	W(E, Eold, a)
	a.measure()

	inverse_qpe(S, E)
	S *= random_U.adjoint()
	qpe(S, E)
\end{minted}
The key difference is that the classical superpower instruction is replaced by
the sub-algorithm described in step 7 of section \ref{sec:QMS}, repeated until
the measured energy matches the energy the system had before the update step. 

