\chapter{The Hubbard model}
\section{Background}
\subsection{The basic Hamiltonian}
The systems studied in this work are a family of systems commonly referred to as
the Hubbard model. In it most basic form, this describes itinerant, 
interacting electrons of $\text{spin-}\frac12$, hopping on a set $\Lambda$
of spatially localized orbitals~\cite{Arovas_2022}. In the framework of second
quantization, the Hamiltonian can be written as:
\begin{equation}
	H = -J \sum_{\langle i,j \rangle,\sigma} \left(c_{i\sigma}^\dagger
	c_{j\sigma} +\text{h.c.}\right)+ U \sum_{i} n_{i\uparrow} n_{i\downarrow}
	\label{eq:basic_hubbard}
\end{equation}
where:
\begin{itemize}
	\item $c_{i\sigma}^\dagger$ and $c_{i\sigma}$ are respectively the creation and
destruction operators for an electron with spin $\sigma$ living in the
$i^{\mathrm{th}}$ orbital
	\item $n_{i\sigma}$ is the number operator for the
electrons with a given $\sigma$ spin and residing in the $i^\mathrm{th}$ site
of $\Lambda$. 
	\item the sum over $\langle i, j \rangle$ is intended over all ``interacting''
		pairs of orbitals
\end{itemize}

The set $\Lambda$ is often a square lattice, and the interacting orbitals pairs
$i, j$ are usually chosen as those pairs that share a nearest neighbour relation.

\subsection{Symmetries}
\subsubsection{U(2) symmetry}
The most apparent symmetry of the \ref{eq:basic_hubbard} hamiltonian is a global
U(2) symmetry:
\begin{equation}
	c_{i\sigma} \rightarrow U_{\sigma\sigma'} c_{i\sigma'}
\end{equation}

The U(2) symmetry can be further separated into two parts: a global U(1)
symmetry and a global SU(2) symmetry. The first reflects global
charge conservation, meaning that the total number of particles in the system is
conserved. Hence, the total particle number $\hat{N} = \sum_{i,\sigma}
\hat{n}_{i,\sigma} = \sum_{i,\sigma} c_{i,\sigma}^\dag c_{i,\sigma}$ is a good
quantum number.

The SU(2) part of the symmetry is reflected in the set of operators:
\begin{equation}
	\boldsymbol{\hat S} = \frac12 \sum_{i,\mu,\nu} c_{i\mu}^\dag\boldsymbol{\sigma}_{\mu\nu}c_{i\nu}
\end{equation}
which represents the total
spin of the system. In particular, its z-component $\hat
S_z=\frac12\sum_i(n_{i\uparrow}-n_{i\downarrow})$ and squared magnitude
$\hat S^2 = \sum_i \hat S_i^2$ are good quantum numbers.

This global U(2) symmetry can be spontaneously broken.
For example, in a ferromagnetic state, the global SU(2) 
symmetry is broken, leading to a non-zero magnetization in the system

\subsubsection{Particle-hole symmetry}

Another useful symmetry present in the case of bipartite lattices is the
particle-hole one. Following the transformation:
\begin{align}
	c_{i\sigma} &\rightarrow \eta_i c_{i\sigma}^\dag \\
	c_{i\sigma}^\dag &\rightarrow \eta_i c_{i\sigma}
	\label{eq:particle_hole_transform}
\end{align}
where $\eta_i = \pm 1$ on alternate sublattices, the hopping term of the
Hamiltonian is unchanged:
\begin{align}
	H_{\text{hop}} 
	&= -J \sum_{\langle i, j \rangle, \sigma} 
		\left(c_{i\sigma}^\dag c_{j\sigma} + \text{h.c.}\right) \\
	&\rightarrow -J \sum_{\langle i, j \rangle, \sigma}  \eta_i \eta_j
		\left(c_{i\sigma} c_{j\sigma}^\dag + \text{h.c.}\right) \\
	&= -J \sum_{\langle i, j \rangle, \sigma} (-1)
		\left(c_{i\sigma} c_{j\sigma}^\dag + \text{h.c.}\right) \\
	&= -J \sum_{\langle i, j \rangle, \sigma} 
		\left(c_{j\sigma}^\dag c_{i\sigma} + \text{h.c.}\right)
		\label{eq:hop_phs}
\end{align}
Where we used the commutation relations \ref{eq:fermion_commutation} and 
the fact that $\eta_i\eta_j = -1$ for any pair of neighbours $i,j$ in a
bipartite lattice.

Using again the commutation relations \ref{eq:fermion_commutation} we get how
the number operator transforms:
\begin{equation}
	n_{i\sigma} = c_{i\sigma}^\dag c_{i\sigma} \quad\rightarrow\quad 
	c_{i\sigma} c_{i\sigma}^\dag = 1 - n_{i\sigma}
	\label{eq:n_phs}
\end{equation}

The Hamiltonian \ref{eq:basic_hubbard} then becomes:
\begin{equation}
	H' = -J\sum_{\langle i,j\rangle,\sigma}\left(c_{i\sigma}^\dag c_{j\sigma} +
	\text{h.c.}\right)+ 
	     U\sum_{i} n_{i\uparrow}n_{i\downarrow} + U (|\Lambda| - N)
\end{equation}

At half-filling, $N = |\Lambda|$, the model is particle-hole symmetric. In such cases,
the system is proven~\cite{Lieb_1993} to be of uniform density, with $\langle
c_{i\sigma}^\dag c_{j\sigma}\rangle = \frac12 \delta_{ij}$ if $i$ and $j$ are
on the same sublattice (with no implicit sum on $\sigma$). 
However, this does not preclude either magnetic
or charge density wave order in the thermodynamic limit ~\cite{Arovas_2022}.

\subsection{Adding the chemical potential}
\subsubsection{Moving to the grand canonical ensemble}
We can relax the condition of keeping fixed the number of particles by moving to
the grand canonical ensemble and by introducing the chemical potential term to
the Hamiltonian:
\begin{equation}
	H = -J \sum_{\langle i,j \rangle,\sigma} \left(c_{i\sigma}^\dagger
	c_{j\sigma} +h.c.\right)+ U \sum_{i} n_{i\uparrow} n_{i\downarrow}
	- \mu \sum_{i,\sigma} n_{i,\sigma}
	\label{eq:chemical_hubbard}
\end{equation}
\subsubsection{Expliciting the particle-hole symmetry}
We can also rewrite this Hamiltonian in a way that makes manifest the
particle-hole symmetry. Let's define $\bar\mu := \mu - \frac12U$. Negleting
constant terms that don't change the physics of the system, we have:
\begin{equation}
	H = -J \sum_{\langle i,j \rangle,\sigma} \left(c_{i\sigma}^\dagger
	c_{j\sigma} +h.c.\right)+ U \sum_{i} 
	\left(n_{i\uparrow}-\frac12\right) \left(n_{i\downarrow}-\frac12\right)
	- \bar\mu \sum_{i} (n_{i\downarrow}+n_{i\uparrow}-1)
	\label{eq:symmetric_hubbard}
\end{equation}

Repeating the particle-hole transformation \ref{eq:particle_hole_transform} on
the Hamiltonian \ref{eq:symmetric_hubbard} and exploiting the mappings
\ref{eq:hop_phs} and \ref{eq:n_phs}, we can write the transformed Hamiltonian:
\begin{equation}
	H' = -J \sum_{\langle i, j \rangle, \sigma} 
		\left(c_{j\sigma}^\dag c_{i\sigma} + \text{h.c.}\right) + \sum_i
	\left(n_{i\uparrow}-\frac12\right) \left(n_{i\downarrow}-\frac12\right)
	+ \bar\mu \sum_{i} (n_{i\downarrow}+n_{i\uparrow}-1)
\end{equation}
The system is therefore symmetric under the inversion of the sign of the
chemical potential $\bar\mu$~\cite{Beckert2013ANS}. We will then limit
ourselves to the study of values of $\bar\mu > 0$, as often done in current
literature~\cite{Arovas_2022}. 

\section{Methods and results from literature}

\subsection{Analytical results}

\subsubsection{Full solutions}
The generic analytical solution to the Hubbard model Hamiltonian is not
known~\cite{Arovas_2022}. Nevertheless smaller models can be solved analytically.

In particular, the Hubbard Hamiltonian is a block-diagonal hermitian matrix due
to the fact that the total number of electrons on the lattice with a given spin
$\sigma \in \{-\frac12,+\frac12\}$ is conserved. This is itself a trivial implication
of the conservation of $S_z$ and $N$. This structure gives us a bound on the
dimension of such submatrices $M_i$ that compose H:
\begin{equation}
	\max(\dim M_i) = \binom{|\Lambda|}{\lfloor \frac{|\Lambda|}{2} \rfloor}^2
	\label{eq:max_hubbard_submatrixes}
\end{equation}

Evaluating this expression numerically yields that the maximum dimension of
blocks on the diagonal of $H$ is 1 for the 1x1 case and 4 for the 2x1 one. These
systems are therefore guaranteed to be solvable through analytical
diagonalization of the Hamiltonian matrix. 

The more complex cases of the generic 1D Hubbard chain~\cite{Lieb_1968} and the 2x2
square~\cite{Schumann_2001} have also been solved analytically. 
For the latter, the energy of the ground state
in the limit of weak and strong hopping $J$ are reported in table
\ref{tab:arovas_2022_hubbardsquare_gs}.

\begin{table}
	\centering
	\includegraphics[width=0.99\textwidth]{Arovas_2022_HubbardSquare_GS.png}
	\caption{Ground state properties for the repulsive Hubbard square. Results
	taken from ~\cite{Arovas_2022}. The properties for $4<N\leq8$ can be obtained
	using the particle-hole symmetry of the system}
	\label{tab:arovas_2022_hubbardsquare_gs}
\end{table}

\subsubsection{Useful theorems}

Besides explicit solutions to the Hubbard problem, there are some useful
theorems that can be applied to the model and provide a theorical description 
of the system for some cases. We are reporting here~\cite{Arovas_2022}, without
their proofs, the most relevant
ones to the purpose of evaluating our simulations against
the predictions of the following theorems.

\begin{theorem}[Lieb's theorem] In the attractive Hubbard model, with $U < 0$,
	if the total number of electrons $N$ is even, then the ground state is
	unique and is a total spin singlet, with $S^2 = 0$~\cite{Lieb_1989}.
	\label{th:lieb}
\end{theorem}
\begin{corollary}[Corollary to Lieb's theorem] In the repulsive Hubbard model,
	with $U > 0$, if the graph $\Lambda$ is bipartite into two subgraphs $A$ and
	$B$, and $N = |\Lambda|$ is even, then the ground state has total spin $|S|
	= \frac12 \big||A| - |B|\big|$, and its degeneracy is only due to the $2S+1$
	spin degeneracy~\cite{Lieb_1989}.
	\label{cor:lieb}
\end{corollary}
In other words, for the common case of a square lattice, this means the
repulsive Hubbard model at half-filling has a total spin close to 0.
\begin{theorem}[Thouless-Nagaoka-Tasaki theorem] For the repulsive Hubbard model
	with $U=\infty, J\geq0$, and with $N=|\Lambda| \pm 1$, the ground state has
	total spin $|S|=\frac12 (|\Lambda|-1)$ and degeneracy $2S+1$.
	\label{th:nagaoka}
\end{theorem}
This establishes that when $U\rightarrow\infty$, there is a ferromagnetic ground
state when the system is one electron away from half-filling.
\begin{theorem}[LSMOH theorem [Lieb-Schultz-Mattis-Oshikawa-Hastings{]}] When the
	filling $n = N/|\Lambda|$ is not an even integer, a unique, gapped,
	featureless, insulating ground state is impossible.
	\label{th:lsmoh}
\end{theorem}
More precisely, in the context of the last theorem, “unique” indicates a single, 
non-degenerate ground state on the torus. “Featureless” means that there is no
spontaneous symmetry breaking of any kind.

\subsection{Phase diagram predictions}

We now report the main findings about the interesting case of
the zero temperature phase diagram, also pictured in figure
\ref{fig:arovas_2022_pd}.

In the limit of weak coupling $U/J \ll 1$, analysis stemming from RG flow
study~\cite{Raghu_2010} have shown that the square lattice has an instability towards
d-wave superconductor.

At half-filling the corollary \ref{cor:lieb} to Lieb's theorem implies that the
system has zero spin. At half filling, there are also classical algorithms that don't
suffer from the fermion sign problem~\cite{Li_2015, Loh_1990} that allow
effective computational approaches.
\begin{figure}
	\centering
	\includegraphics[width=0.7\textwidth]{loh_1990_6b.png}
	\caption{Low temperature average double occupancy at $J=1$. Data taken from
	ref. ~\cite{Loh_1990}. In the weak coupling limit $U/J \ll 1$ the system
	behaves as two independent layers, each of which is half-filled. Therefore,
	the average double occupancy must go to $\left(\frac12\right)^2=\frac14$. 
	In the strong coupling limit $U/J \gg 1$, the larger energy associated with the
	occurence of double occupancy ``discourages'' its occurrence, and given the
	existence of states with no double occupancies, it is expected that as
	$U\rightarrow\infty$, $\ev{n_\downarrow n_\uparrow}\rightarrow0$.
	}
	\label{fig:loh6b}
\end{figure}

At half-filling and strong coupling $U/J \gg 1$ the
Hubbard model is known~\cite{Arovas_2022} to behave as an anti-ferromagnetic insulator.

In the strong coupling limit $U\rightarrow\infty$, when the system is just one
electron away from half-filling, the Nagaoka theorem \ref{th:nagaoka} predicts a
fully ferromagnetic behaviour. It is then clear that the limits
$U\rightarrow\infty$ and $|\Lambda|\rightarrow\infty$ do not commute. If the limit
$U\rightarrow\infty$ is performed first, one falls into the ferromagnetic
scenario described by the Nagaoka theorem. By contrast, if the limit
$|\Lambda|\rightarrow\infty$ is done first, then the chosen configuration is
infinitely close to the antiferromagnetic case to which applies the corollary to Lieb's theorem.

It can be shown~\cite{Arovas_2022} that the transition between these two
scenarios involves an intermediate region of the phase diagram where the two
macroscopic phases coexist.

The region of stability for the half-metallic ferromagnetic (HMF) configuration
that follows at greater electron densities the Nagaoka scenario has
not been rigorously established~\cite{Arovas_2022}. Still, it has been shown
that:
\begin{itemize}
	\item even at $U=\infty$, there exists a critical density $n_c > 1$ beyond
		which the HMF configuration is unstable~\cite{Shastry_1990};
	\item at $U=\infty$ the HMF is stable in the range $1<n<n_c\approx1.2$ and
		the two phase coexistence happens in a range of densities
		$1<n<n_c'\sim(t/U)^{1/2}$~\cite{Emery_1990,Liu_2012}.
	\item there exists a cutoff $U_\text{cutoff}$ under which the ferromagnetic
		phase is unstable~\cite{Arovas_2022}.
\end{itemize}
%``Dilute limit'' is a Fermi liquid with possible superconducting instabilities
% Dilute limit non si capisce un cazzo, ogni metodo dice di tutto e di più

Direct approaches with classical Monte Carlo technique are possible but the sign
problems limits current best attempts to small 2d 8x8 lattices, at temperatures
$T/J > 0.2$. In these conditions, finite size effects are small enough that an
8x8 lattice is probably enough to make inferences about the termodynamic limit,
but the temperatures are too high to make solid predictions about the ground
state of the system~\cite{Arovas_2022}.

\begin{figure}
	\centering
	\begin{subfigure}{.43\textwidth}
		\centering
		\includegraphics[width=\textwidth]{Arovas_2022_PD_x.png}
		\caption{}
		\label{fig:arovas_2022_pd_x}
		\vspace{7pt}
	\end{subfigure}
	\begin{subfigure}{.41\textwidth}
		\centering
		\includegraphics[width=\textwidth]{Arovas_2022_PD_mu.png}
		\caption{}
		\label{fig:arovas_2022_pd_mu}
	\end{subfigure}
	\caption{
		Speculative phase diagram at zero temperature of the Hubbard model, as a
		function of the $U/t$ ratio, and $x$, the doping hole density, or $\mu$,
		the chemical potential.
		``AFI indicates an insulating (incompressible) phase with Néel AF order
		and doped hole-density x = 0. The thick solid line indicates a first-order
		transition from the AFI to a conductive phase, 
		and the black circles denote points at which x = 0 in the conductive phase.
		“2-phase” denotes a region of two-phase coexistence. HMF denotes a half
		metallic ferromagnetic phase. At intermediate U/8t, 
		there is clear numerical evidence of multiple local ordering
		tendencies of comparable strengths including unidirectional CDW [charged
		density waves],	colinear unidirectional SDW [spin density waves], 
		nematic, and d-wave SC. 
		Which of these phases actually orders is still uncertain.''
		Taken from ref.~\cite{Arovas_2022}.
	}
	\label{fig:arovas_2022_pd}
\end{figure}

