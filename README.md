# My work for my final exam @SNS

![build status](https://gitlab.com/1998marcom/esame-licenza-sns/badges/master/pipeline.svg?style=flat)

Latest slides pdf downloadable [here](https://gitlab.com/1998marcom/esame-licenza-sns/-/jobs/artifacts/master/raw/build/EsameLicenzaSNS-Malandrone-slides.pdf?job=build).

Latest presentation speech pdf downloadable [here](https://gitlab.com/1998marcom/esame-licenza-sns/-/jobs/artifacts/master/raw/build/EsameLicenzaSNS-Malandrone-speech.pdf?job=build).

