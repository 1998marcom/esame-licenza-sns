\documentclass[a4paper,12pt]{article}
\usepackage[outer=2cm,inner=2.5cm,top=3cm,bottom=2.5cm]{geometry}

\title{Quantum Computation for the Hubbard model\\{\large Presentation speech}}
\author{Marco Malandrone}
\date{\today}

\begin{document}
\maketitle
\tableofcontents

\section{Sign problem}
Come introduzione ho scelto di partire da queste tre immagini. 
Rappresentano: il nucleo di un atomo, un materiale
superconduttore ad alta temperatura, e in particolare un cuprato, e
infine in basso a sinistra c'è una rappresentazione artistica di una stella di
neutroni. Una cosa che questi tre sistemi hanno in comune è il fatto che non
siamo in grado al giorno d'oggi di offrire simulazioni numeriche accurate che
li descrivano. Questo è dovuto al fatto che in tutti e tre gli scenari abbiamo a
che fare con fermioni fortemente interagenti, le cui simulazioni sono ostacolate
dal cosiddetto ``problema del segno''. 

Un modo per inquadrare il problema del segno è quello di partire dal problema
del calcolo delle medie termiche degli osservabili di un sistema. Il modo
tradizionale di affrontare questo tipo di problemi, nel caso dei sistemi
quantistici, è quello di scrivere la funzione di partizione come [traccia di e alla meno
beta H], e poi utilizzare questa per calcolare la media termica di un generico
osservabile A: [traccia di A per e alla meno beta H diviso Z, la funzione di
partizione]. A questo punto fare la traccia esplicitamente è numericamente non
fattibile nella maggior parte dei casi, quindi l'idea è di costruire un Monte
Carlo che ci dia un modo di estrarre dei campioni dall'insieme di tutte le
possibili configurazioni del sistema, secondo la distribuzione termica di
Boltzmann. Estratti questi campioni, possiamo misurare su di loro gli
osservabili e ricavare le medie termiche. Come si fa questo in pratica?
Un modo è quello di espandere [e alla meno beta H] in serie di Taylor
e inserire una base ortonormale di stati prima di ogni H. A questo punto le
nostre configurazioni sono date da un certo insieme di stati [x con zero, x con
uno, x con enne], e a ognuna di queste possiamo associare un ``peso'' [wu
doppio]. Se questi pesi sono tutti positivi, allora possiamo costruire un Monte
Carlo dove la probabilità di campionare ciascuna configurazione è proporzionale
a [wu doppio].

Il problema che sorge è che però non sempre questi pesi [wu doppio] sono
positivi. In particolare per molti sistemi di fermioni interagenti, come quelli
menzionati all'inizio, questi pesi [wu doppio] possono anche essere
negativi, e non è più quindi possibile affermare che le probabilità di
campionare una configurazione siano proporzionali a [wu doppio]. Esistono
workaround generali: ad esempio, si può sostituire un peso negativo con il suo valore
assoluto, e trasferire il segno negativo alla misura dell'osservabile. Questo
procedimento, come tutti gli altri workaround, introduce un errore nelle misure.
E in particolare si tratta solitamente di un errore che scala esponenzialmente
con le dimensioni del sistema simulato ed esponenzialmente anche con l'inverso
della temperatura. In altre parole, questi workaround, non funzionano bene per
sistemi grandi e a bassa temperatura. Un altro modo per affrontare questo
problema del segno è invece quello di sviluppare degli algoritmi specifici per
ciascun sistema. Talvolta magari scegliere una base furba o sfruttare qualche
legge di conservazione consente di affrontare il problema evitando il problema
del segno. Una vera soluzione generale per tutti i sistemi è, però, improbabile.
Infatti, è stato dimostrato da Troyer[pronuncia?] e Wiese[pronuncia?] che il
problema del segno è un problema NP-hard, ovvero una sua soluzione in tempo
polinomiale implicherebbe [P uguale NP], cioè esisterebbero soluzioni polinomiali a problemi
come trovare una password datone l'hash o trovare una chiave privata avendo a disposizione 
solo la chiave pubblica. Ora, anche se non è dimostrato che [P è diverso da NP],
ci sono motivi per ritenere che sia così, e quindi una soluzione generale, in
tempo polinomiale, del problema del segno è improbabile. 

\section{Quantum Metropolis Sampling}
Il fatto che una soluzione del tutto generale sia improbabile non impedisce però di
trovare soluzioni che siano applicabili comunque a molti casi. In particolare, una di
queste è data dal cosiddetto algoritmo del Quantum Metropolis Sampling. Questo algoritmo è
disegnato per funzionare su un computer quantistico, ed è applicabile
efficacemente a una larga parte dei sistemi fisici. In particolare una
condizione necessaria per la sua applicabilità è il fatto che dobbiamo essere in grado di applicare
efficientemente l'operatore di evoluzione temporale [e alla meno i H t]. 
Questo, come dimostrato da Lloyd, è fattibile
per la maggior parte dei sistemi con interazioni locali, ovvero la quasi
totalità dei sistemi fisici.
Detto questo, il fatto che per un sistema sappiamo simulare efficientemente
l'evoluzione temporale non garantisce che l'algoritmo del QMS sia efficiente nel
campionare un dato sistema. Vi possono essere altri problemi che rendono la
simulazione inefficiente. Ad esempio, nel sistema considerato da Troyer e Wiese
per dimostrare che il problema del segno è NP-hard, la presenza di tanti minimi
locali rende il QMS inefficace. 
Qual è quindi l'idea di base del QMS? L'idea è
quella di sfruttare una particolare subroutine che consente di aggirare
completamente il problema, nel senso che non espandiamo più in serie di Taylor
[e alla meno beta H], ma valutiamo direttamente l'energia, determiniamo se lo
step di evoluzione del Monte Carlo viene accettato o no cercando di minimizzare
le interferenze sul sistema, e poi, quando lo step di evoluzione viene
accettato, misuriamo l'energia e facciamo collassare il sistema in un autostato
dell'hamiltoniana.

Adesso entriamo un attimo più nel dettaglio di come funziona l'algoritmo del
QMS. La prima subroutine che vale la pena di menzionare è la Quantum Fourier
Transform. Se, preso un registro con n qubit, definiamo la base di Fourier come
mostrato in equazione 6, allora la trasformata di Fourier altro non è che quella
evoluzione unitaria che manda ogni vettore X della base computazionale nel suo
corrispondente vettore della base di Fourier. Diamo per buona l'esistenza di
un'implementazione di questa subroutine con complessità
computazionale dell'ordine di [n quadro], con n il numero
di qubit del registro.

L'altra subroutine importante del QMS è Quantum Phase Estimation, o QPE. Qual'è
il problema risolto dalla QPE? Supponiamo di:
- avere un operatore U unitario che può agire su un registro ad n qubit, di modo
che ci sia un particolare autostato [u piccolo] con autovalore [e alla i teta],
dove teta è un valore ignoto, compreso comunque tra zero e [due pi greco].
Supponiamo anche di avere un registro di ``sistema'' ad n qubit proprio inizializzato
nell'autostato [u piccolo], e di avere un altro registro dell'``energia'' ad r
qubit. Richiediamo infine di poter implementare efficientemente delle [control-U
alla 2 alla j], per j appartenente a [zero, uno, due, ..., r-1].
Dato questo setting, l'obiettivo che raggiunge la QPE è quello di di dare la
migliore stima ad l bit per [teta su due pi greco], dove necessariamente l è
minore o uguale a r.

Bene. Si può dimostrare che il problema è risolto dal circuito in figura, dove
abbiamo r [control-U alla due alla j] e una QFT inversa alla fine sul registro
dell'energia. La complessità computazionale della QPE è quindi [O di r quadro]
più la complessità legata all'applicare r volte queste control U. Nella maggior
parte dei casi di nostro interesse, il costo di implementare una control U è
maggiore di [O di r] e quindi la complessità computazionale totale dipende solo
da quanto efficientemente sappiamo applicare le control U.

Un caso speciale su cui vale la pena un attimo di soffermarsi è quello in cui la
matrice U di cui vogliamo trovare gli autovalori sia espressa tramite il teorema
di Stone, ovvero sia data dall'esponenziale di una matrice hermitiana nota H. In
questo caso l'applicazione della QPE altro non è che uno strumento per poter
misurare [H su due pi greco]. Appunto non banale: se noi conosciamo solo H, e
non U, come possiamo applicare U?. Fare l'esponenziale della matrice è
computazionalmente non sostenibile, quindi quello che si fa è spezzare
l'evoluzione di un tempo t in tanti step piccolini, lunghi delta, e poi dividere
l'Hamiltoniana in una somma di tante piccole sub-hamiltoniane a due qubit. Un
a volta fatto questo, l'errore che facciamo applicando le sub-hamiltoniane
separatamente, anziché tutte insieme, è dell'ordine di [delta quadro] per ogni
step lungo delta, ovvero compiamo un errore finale di [O di delta].

Veniamo ora al Quantum Metropolis Sampling vero e proprio. Prendiamo un registro di ``sistema'' ad n
qubit S. Supponiamo di poter implementare efficientemente [e alla i H t].
Prendiamo un registro a l qubit dell'``energia'' E. Prendiamo un singolo qubit
per un registro dell'``accettanza''. Infine, nella proposta originale del QMS si
fa uso di un quarto registro per memorizzare l'energia dello step precedente del
Metropolis. In realtà, è possibile sostituire questo registro quantistico con un registro
classico, sempre ad l bit, che chiameremo [E old].
L'obiettivo del QMS è quello di campionare lo stato del sistema secondo la
distribuzione di Boltzmann indotta sul sistema dall'hamiltoniana H.

Nel concreto, quindi, come funziona l'algoritmo del QMS? Prima parte: partiamo
da uno stato iniziale in cui il sistema si strova in un generico stato,
scrivibile in termini di autostati dell'hamiltoniana. I registri dell'energia e
dell'accettanza sono impostati a 0 e il registro E old è non inizializzato.
Preso questo stato iniziale, facciamo una Quantum Phase Estimation tra il
registro S e il registro E, misuriamo l'energia, la memorizziamo in E old, e
resettiamo il registro dell'energia. Notiamo che con la misura dell'energia lo
stato del sistema è collassato su un autostato dell'hamiltoniana.

A questo punto prendiamo una matrice unitaria random U e la applichiamo al
nostro registro di sistema S, facciamo un'altra QPE tra il registro di sistema S
e il registro dell'energia E, ma non misuriamo l'energia. Calcoliamo invece
quantisticamente la probabilità di accettare lo step del Metropolis con un
operatore che ci porti nello stato riportato al punto 6. 

A questo punto misuriamo il qubit dell'accettanza: se la misura ha come esito 1,
l'evoluzione è accettata e possiamo ricominciare dallo step 1, altrimenti usiamo
una procedura per riportare il sistema ad un autostato di energia E old.
Appliciamo l'inverso delle operazioni della slide precedente e usiamo una QPE
per assicurarci che l'energia dello stato sia effettivamente di nuovo pari ad
E old. Se così non è riapplichiamo le operazioni della slide precedente,
misuriamo l'accettanza e, indipendentemente dal risultato, applichiamo l'inverso
delle operazioni della slide precedente e usiamo di nuovo la QPE per misurare
l'energia, fin quando non raggiungiamo l'obiettivo di tornare ad avere un
energia E old.

Quali sono le computazioni dispendiose per il Quantum Metropolis Sampling? La
Quantum Phase Estimation e potenzialmente la matrice random U. È però possibile
scegliere la matrice random da un sottinsieme di operatori che sono
implementabili efficientemente, e quindi il fattore che limita la possibilità di
applicare il QMS è semplicemente il fatto di dover applicare la QPE, per cui
dobbiamo poter simulare efficientemente l'hamiltoniana. È questo il caso,
fortunatamente, della maggior parte dei sistemi fisici.

Aggiungiamo ora una postilla a quanto detto finora. Il QMS ha delle sorgenti di
errore anche nel caso in cui venisse eseguito su un computer quantisico ideale.
In particolare, vi sono dei tempi di ritermalizzazione del sistema, l'energia
viene memorizzata con un numero finito di cifre, gli stati possono essere
rappresentati solo da un numero finito di qubit, e la trotterizzazione
dell'evoluzione temporale porta con sé degli errori.

\section{Hubbard}
Mettiamo da parte quanto detto fin ora e cerchiamo un modello fisico con cui
possiamo sperimentare ora con il QMS. Le richieste che abbiamo sono: 
1. il sistema in questione deve essere ragionevolmente simulabile, cioè non
vogliamo dover impiegare più di una ventina di qubit per l'intero algoritmo. 2.
Il sistema deve essere di interesse fisico. 3. Ci devono essere dei risultati
noti con cui poter confrontare i risultati. 4. Il modello deve soffrire del
problema del segno, cosa che rende il QMS uno strumento utile per affrontarlo.
Un problema che spunta tutti le caselle della nostra checklist è il modello di
Hubbard: 1. Il modello più piccolo ci richiede di usare 9+$l$ qubit, dove $l$ è
il numero di qubit del registro dell'energia. 2. Il modello di Hubbard è il
modello paradigmatico dello studio dei sistemi di elettroni fortemente
interagenti. 3. C'è molta letteratura a suo riguardo. 4. Per alcuni valori dei
parametri dell'hamiltoniana, il modello di Hubbard soffre del problema del
segno.

Ma cos'è allora il modello di Hubbard? Il modello di Hubbard descrive un insieme
di elettroni, o di particelle di spin [un mezzo], che si muovono su un grafo.
Solitamente questo grafo è un reticolo e, in particolare, un reticolo quadrato.
L'Hamiltoniana del sistema è quella riportata in equazione 14, e ha un termine di
hopping proporzionale a J e un termine di interazione spin-up spin-down
proporzionale a U.

Il modello dispone di due simmetrie importanti. La prima è una simmetria globale
U(2), divisibile in una simmetria globale U(1), legata alla conservazione del
numero di particelle, e una simmetria globale SU(2) legata allo spin globale.
Un'altra simmetria del sistema è la simmetria lacuna-particella, realizzata
dalla trasformazione 16, e che porta all'hamiltoniana un termine aggiuntivo che
dipende da solo dal numero di particelle e si annulla nella situazione di half
filling o mezzo riempimento. 

Menzioniamo ora due teoremi importanti che consentono di descrivere in alcuni
limiti il ground state del modello di Hubbard. Il primo risultato è il
corollario di un teorema dovuto a Lieb, e afferma che, ad half filling, se un
grafo è bipartito, allora il ground state ha spin totale dato dalla differenza
delle cardinalità dei due subgrafi, e la degenerazione è solo dovuta allo spin.
In altre parole, nel limite termodinamico, per un reticolo quadrato, ad
half-filling, il sistema ha spin totale zero.

Il secondo teorema è dovuto a Nagaoka, Thouless e Tasaki e afferma che quando U
è infinito, J è non negativo, e siamo una lontani una particella dall'half
filling, lo stato fondamentale è ferromagnetico, e l'unica
degenerazione è sempre dovuta allo spin. In particolare, il modello 2x2 di
Hubbard è stato risolto analiticamente da Schumann, che ha trovato una soglia
critica di U oltre la quale abbiamo come ground state questa fase
ferromagnetica. Questo valore critico di U si ha quanto il rapporto U/J è di circa
19.

Ok finalmente un'immagine. In questa immagine è riassunto lo stato di conoscenza
attuale sul ground state del modello di Hubbard, al variare del rapporto [U su
J] e del riempimento $x$, dove $x$ va da zero, ad half-filling, a 1, quando il 
riempimento è completo. Ad [x uguale a zero] esiste un algoritmo che consente di
aggirare il problema del segno, ad il diagramma di fase è quindi noto. Lungo la
linea a sinistra del grafico quindi, per valori piccoli di U/J il sistema si 
comporta come un superconduttore non magnetico, mentre per alti valori di U/J il 
sistema è antiferromagnetico ed è un isolante di Mott.
Leggermente lontano dall'half-filling, per valori di U molto grandi, il sistema
si comporta come un ferromagnete conduttore solo per elettroni con un certo
spin. La regione antiferromagnetica e quella ferromagnetica sono raccordate da
una regione intermedia di coestistenza delle due fasi a livello macroscopico.
Nella regione con valori di riempimento più elevati il diagramma di fase è molto
più incerto, e diversi studi hanno raggiunto conclusioni diverse. 

Un'ultima aggiunta che vogliamo fare a quanto detto sul modello di Hubbard è il
fatto che è ovviamente possibile studiare il sistema anche nel framework
dell'ensemble gran canonico introducendo il potenziale chimico. 
La simmetria U(2) è inalterata, mentre la particle
hole simmetry implica che possiamo limitarci a studiare una situazione in cui il
doping rispetto all'half filling si ha solo con elettroni, sapendo che i
risultati sono uguali per il doping con lacune. Il diagramma di fase speculativo in
funzione di mu è riportato in figura b.

\section{Errors}
Passiamo ora ad analizzare quelli che sono gli errori che ci portiamo appresso
con il QMS. Una prima classificazione è: quali delle sorgenti di errore che
abbiamo menzionato prima portano ad errori rilevanti? La risposta è: il tempo
finito di ritermalizzazione e sopratutto il fatto che l'energia venga
digitalizzata con pochi qubit. Un altro errore che è necessario controllare è
inoltre quello associato alla simulazione classica dell'evoluzione temporale.
Non sono invece sorgenti di errori rilevanti il fatto che lo stato del sistema
sia digitalizzato, perché lavoriamo con un sistema quantistico con un numero
finito e piccolo di gradi di libertà, e non contribuisce all'errore la
trotterizzazione dell'evoluzione temporale, perché la simuliamo direttamente a
livello classico.

Partiamo subito dagli effetti che ha un tempo di ritermalizzazione finito: 1. ci
sono delle correlazione indesiderate fra le misurazioni, similmente a quanto
accade con l'algoritmo di Metropolis classico, e 2. ogni volta che facciamo una
misura sul sistema che non commuta con l'hamiltoniana distruggiamo lo stato del
sistema. Un primo modo di provare a stimare quanto valga il tempo di
correlazione possiamo si può avere dal grafico in figura, che riporta la
correlazione temporale dell'energia al variare del numero di step del QMS
compiuti. Se chiediamo che la correlazione scenda a valori compatibili - in
valore assoluto - con quelli per $\tau$ molto grandi, allora vuol dire ottenere 
un tempo di ritermalizzazione di circa 15. 

Abbiamo inoltre misurato gli osservabili di nostro
interesse per diversi tempi di ritermalizzazione e sono qui mostrati in funzione del
numero di step del QMS effettuati. In figura qui sono riportati i
risultati più emblematici, con un tempo di ritermalizzazione di uno step QMS o
di 15 step del QMS.

Altro problema di primaria importanza è la digitalizzazione dell'energia, che ha
due effetti principali: 1. la rappresentazione dell'energia è necessariamente
inaccurata. 2. ci sono degli step del Metropolis ``indesiderati''. Partiamo dal
guardare come si comportano le medie delle misure al variare del parametro J
quando il numero di qubit dell'energia passa da [l uguale 4] a [l uguale 6]
qubit. Spoiler: sappiamo che il risultato analitico giusto prevede che
l'osservabile arancione vada a zero quando x va a 0 e sappiamo che l'osservabile
blu deve andare a 0 per x grandi, almeno per il ground state. Un risultato
evidente dell'attaccare il problema con meno qubit del necessario è il fatto che
il sistema si comporta come se avesse una temperatura maggiore di quella che noi
vorremmo richiedere con il QMS. In una delle appendici del mio lavoro di tesi, infatti, si
può trovare un esempio analitico di come usare pochi qubit sia proprio interpretabile
esattamente in questo modo.

Voglio ora dare un idea del perché ci sono delle transizioni particolarmente
indesiderate quando digitializziamo l'energia nel modo più naive possibile.
Prendiamo un reticolo con spazio per 8 fermioni non interagenti, in cui l'unico
contributo all'energia è dato dal potenziale chimico. Fissiamo il numero di 
qubit dell'energia e facciamo in modo che l'autovalore massimo dell'energia 
corrisponda al valore 1111 in binario. A questo punto guardiamo qual è la 
probabilità di misurare una certa energia per il primo stato eccitato. Quello
che si vede è che l'energia è sì piccata intorno al valore corretto, 1, però
questo picco si estende sui valori di energia vicini, quindi 0, 2, -1. Il
problema è che stiamo guardando le cose modulo 8, quindi abbiamo una probabilità
concreta di avere dei salti di energia da 1 ad 7, al valore massimo. Un modo per
evitare questo problema è quello di rinunciare ad utilizzare i valori estremali
dell'energia e lavorare con un intervallo più piccolo di valori utili.

Ad esempio, in questa immagine sono riportati i risultati ottenuti dal modello
di Hubbard, in cui non è stato usato nessun accorgimento per ridurre il problema 
di questi salti energetici dal ground state ad energia massima.

Qua invece vi è la stessa simulazione, in cui però abbiamo rinunciato ad usare
11 livelli estremali, con il risultato di avere eliminato buona parte di questi
``salti''.

In ultima istanza vale la pena menzionare il fatto che, anche se, dato che siamo
su un simulatore classico, possiamo evitare il problema della trotterizzazione
nel senso quantistico, abbiamo comunque necessità di spezzare il tempo in tanti
piccoli intervalli. Infatti l'algoritmo classico di Krylov-Arnoldi, che consente di tenere
la complessità computazionale nell'ordine di [2 alla n], richiede per motivi di
stabilità numerica che il prodotto tra la lunghezza dello step temporale e
l'autovalore massimo di H sia al più dell'ordine dell'unità.

\section{Results}
Bene, veniamo al nocciolo della tesi: le nostre simulazioni. Abbiamo sempre
simulato un modello di Hubbard bidimensionale [2 per 2], con 6 qubit per il
registro dell'energia. Il numero di step QMS che abbiamo eseguito è di 10000 per
temperature basse, con [beta uguale 50], e di 4000 per le altre temperature più
elevate. Gli osservabili che abbiamo misurato, oltre all'energia, sono 3: il
doppio della media dello spin lungo z al quadrato, la doppia occupazione media e 
la densità di elettroni, traslata in modo che ad half-filling la densità sia 0.

Siamo proprio partiti dalla situazione di half-filling impostando il potenziale
chimico a [U mezzi]. Dalla letteratura disponibile, sappiamo che per valori di
U/J bassi dobbiamo avere un conduttore: questo è verificato dal fatto che la
doppia occupazione tende a un quarto, e dobbiamo avere una situazione senza
senza magnetizzazione, e anche questo è verificato dallo spin nullo. Ad alti
valori di U/J ci aspetteremmo un isolante, ok, infatti la doppia occupazione va
a 0, ma ci aspetteremo anche un materiale antiferromagnetico. Qua però il
quadrato dello spin lungo zeta si avvicina al valore che avrebbe in una
situazione ferromagnetica, ovvero un terzo. Qusto non torna. Un possibile motivo
è legato alla combinazione della maggiore molteplicità degli stati
ferromagnetici e alla ruvida discretizzazione dell'energia.

A questo punto abbiamo esplorato le regioni più lontane dalla situazione di half
filling. I grafici sono simili, ma qua sì che ci aspettiamo una possibile fase
ferromagnetica ad alti valori di U/J, come previsto dal teorema di Nagaoka. Un
altro dato interessante che emerge da queste due figure è che la transizione di
fase che prima era netta ora sembra dividersi in due step, con una regione
intermedia che si allontana facilmente dall'half filling, mentre per valori
estremali di U/J l'occupazione rimane pressoché inchiodata all'half-filling.

A questo punto esibiamo dei colorplot che mostrano intuitivamente le
diverse fasi del modello di Hubbard che abbiamo trovato. Come lo costruiamo?
Prendiamo i tre osservabili misurati, la doppia occupazione, lo spin z quadro e
la densità elettronica e mappiamo ciascun osservabile in un canale diverso di un
immagine RGB. Quali i parametri che variamo nell'esplorazione del diagramma di
fase? J e mu, e quindi anche U/J e mu. Non riportiamo le incertezze ma un'idea
del loro ordine di grandezza è data dalle fluttuazioni nel grafico.
In questo grafico facciamo variare J linearmente, che è una possibilità
legittima. Ma se siamo interessati a confrontare i risultati con la letteratura
ci possiamo uniformare allo standard di mettere sull'asse delle ordinate $U/J$.

Quindi facciamolo e guardiamo bene infaccia le tre regioni che osserviamo. 1. La
regione verde ferromagnetica che corrisponde correttamente alla regione HMF attesa, ma che
prende anche il posto di AFI, la quale non ha una regione corrispondente
antiferromagnetica nelle nostre simulazioni.un ferromagnete isolante.
Troviamo poi la regione rosso bruno in basso, dove
abbiamo un conduttore non ferromagnetico ad half-filling.
Entrambe queste regioni sfumano poi a destra nella regione viola,
una regione fortemente dopata, ferromagnetica che corrisponde alle regioni del diagramma di
fase più incerte della letteratura.

Abbiamo poi provato ad aumentare la temperatura del sistema e vedere cosa
succede. In particolare qua sono confrontate le simulazioni a [beta uguale 50]
con quelle a [beta uguale 4]. Il grosso cambiamento che abbiamo all'aumentare
della temperatura è il fatto che le misurazioni si appiattiscono verso il valore
entropicamente più favorito. Quindi la densità misurata si sposta verso l'half
filling, la correlazione massima scende quindi, perché ci sono meno particelle,
mentre quella minima sale, perché, anche ad half-filling, il valore
entropicamente favorito è quello di un quarto, 0.25. Gli stati ferromagnetici si
mischiano con quelli non ferromagnetici e il risultato è che anche lo spin
massimo misurato scende considerevolmente da ~0.4 a meno di 0.25. 

Lo stesso trend continua poi nelle simulazioni a temperatura più alta ancora,
qua ci sono riportati i dati a [beta uguale 2],

beta uguale uno e beta uguale 0.5.

\section{Conclusion}
In conclusione, cosa abbiamo fatto? Abbiamo calcolato le medie termiche di
osservabili per un piccolissimo modello di Hubbard, abbiamo ottenuto dati
compatibili con lo stato dell'arte per il ground state, modulo la fase
antiferromagnetica. Sono importanti i risultati a livello quantitativo? No.
Cos'è rilevante? È rilevante il fatto di aver testato il QMS su un modello che
soffre del problema del segno e aver visto che non ci sono ostacoli particolari,
modulo di nuovo la fase antiferromagnetica, al raggiungimento di risultati
convincenti. A priori infatti ci sono casi in cui, anche se l'hamiltoniana è
simulabile su un computer quantistico, il QMS si inceppa. Un esempio, come detto
in precedenza, è la situazione in cui vi sono molti minimi locali.
In che direzione è opportuno proseguire il lavoro ora? Un primo step immediato
può essere quello di affrontare il problema nello stesso modo ma provare a
dedicare più risorse computazionali, per provare ad affrontare reticoli più
grandi o magari dedicare più qubit al registro dell'energia per andare a
risolvere anche il problema della fase antiferromagnetica. Un'altra direzione
particolarmente interessante è legata all'esplorare come si comporta il QMS,
specie a bassa temperatura, con dei diversi mapping dall'hamiltoniana fisica
all'hamiltoniana che usiamo nel QMS. In modo simile, ma sotto un profilo
decisamente più teorico, vi è la possibilità di investigare se vi è o no la
possibilità di costruire un algoritmo analogo alla quantum phase estimation, e
quindi poi un quantum metropolis sampling, dove però il registro dell'energia
utilizza un encoding sulla falsariga dei floating point classici. Questo
risolverebbe i problemi di risoluzione alle basse energie e eliminerebbe la
necessità di stimare il minimo e massimo autovalore dell'hamiltoniana prima di 
utilizzarla nel QMS. Quarta idea è la possibilità di testare e 
confrontare i risultati con altri algoritmi quantistici, come il variational quantum
eigensolver, che serve per trovare il ground state di un sistema quantistico, e
sono le proprietà del ground state ad essere particolarmente interessanti per
molti sistemi fisici, in generale tutti quelli in cui la temperatura tipica del
sistema è molto più piccola del gap energetico tra il ground state e il primo
stato eccitato. Infine, cosa più importante di tutte: questo algoritmo, lo possiamo
far girare su un computer quantistico oggi? No. Infatti anche essendo mooolto
generosi e stimando al ribasso il numero di gate necessari, ogni QMS step del
modello di Hubbard 2x2 ha bisogno di almeno [10 alla 4] gates, mentre i computer
quantistici attuali forniscono risultati inattendibili quando il circuito
impiega più di [10 alla 3] gate. Si può provare a combinare dell'error
correction code, ma anche in questo caso la qualità degli attuali computer
quantistici non è sufficiente. Questo però è un limite oggi, lo sviluppo dei
computer quantistici prosegue e il giorno in cui potremo provare il QMS su una
macchina reale potrebbe non essere così lontano.


\end{document}
