\documentclass{beamer}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                                %%
%%             HEADER             %%
%%                                %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage{anyfontsize}


%%%%%%% BEAMER SETTINGS
\usetheme[width=2.0cm]{Berkeley}   % bianco-azzurro, molto leggero. 
\usecolortheme{seahorse}

\setbeamertemplate{title page}[default][rounded=true,shadow=false]
\setbeamercolor{frametitle}{fg=structure,bg=white}


\makeatletter
\setlength{\beamer@headheight}{0pt}
\makeatother

%\setbeamertemplate{frametitle}[default][center]   % centrare il titolo 

\setbeamertemplate{enumerate items}[circle]
\setbeamertemplate{itemize items}[circle]
%\setbeamertemplate{section in toc}[circle]

\setbeamertemplate{section in toc}[circle]
\setbeamerfont{section in sidebar}{size=\fontsize{7}{10}}
\setbeamerfont{author in sidebar}{size=\fontsize{8}{9}}
\setbeamerfont{title in sidebar}{size=\fontsize{10}{11}}
\setbeamertemplate{frametitle}{
	\vspace*{3mm}
	\insertframetitle
}


%%%%%%% PACKAGE IMPORTS
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{graphicx}
\usepackage{xcolor}
\usepackage{pbox}
\usepackage{url}
\usepackage{physics}
\usepackage{qcircuit}
\usepackage[normalem]{ulem}
\usepackage{subcaption}
\usepackage{array}
\usepackage{multirow}
\usepackage{multicol}
%\usepackage{nicematrix}      % matrici con tabelle belline
%\usepackage{tensor}            % tensori
%\usepackage{listings}           % fare del codice in tabella

%%%%%%% BIBLIOMERDA
\usepackage[
    backend=biber,
    %style=numeric,
    %citestyle=phys,
	bibstyle=authoryear,
    sorting=none
]{biblatex}

\addbibresource{slides/bibliography.bib} %Imports bibliography file
%\setbeamertemplate{bibliography item}{\insertbiblabel}
\renewcommand*{\bibfont}{\normalfont\footnotesize}

%%%%%%% TIKZ
\usepackage{tikz}
\usetikzlibrary{math}
\usetikzlibrary{arrows.meta}
\usetikzlibrary{positioning}

%%%%%%% CUSTOM MACRO
\newcommand{\eps}{\varepsilon}
\newcommand{\one}{\mathds{1}}
\newcommand{\RED}[1]{\textcolor{red}{#1} }


%%%%%%% MAKETITLE INFO
\title[QC for the Hubbard model]{Quantum Computation\\for the Hubbard
model\footnote{\scriptsize \href{https://gitlab.com/1998marcom/esame-licenza-sns}{gitlab.com/1998marcom/esame-licenza-sns}}}
\subtitle{}

\author[Marco Malandrone]{Marco Malandrone}
\institute[SNS]{
	Scuola Normale Superiore
}
\date{July 25, 2023}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                                %%
%%              BODY              %%
%%                                %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}
	
\frame{\titlepage}

\section{Sign problem}

\begin{frame}
	\frametitle{Three pictures}
	\begin{figure}
		\centering
		\begin{subfigure}{0.3\textwidth}
			\centering
			\includegraphics[width=0.7\textwidth]{images/atomicNucleus.png}
			\caption{Atomic nucleus}
			\vspace{30pt}
		\end{subfigure}
		\hfill
		\begin{subfigure}{0.5\textwidth}
			\centering
			\includegraphics[width=0.9\textwidth]{images/highTempSuperconductor.jpg}
			\caption{High-temp superconductor}
		\end{subfigure}
		\hfill
		\begin{subfigure}{0.5\textwidth}
			\centering
			\vspace{-30pt}
			\includegraphics[width=\textwidth]{images/neutronStar.jpg}
			\caption{Neutron star}
		\end{subfigure}
		\begin{subfigure}{0.48\textwidth}
			\makebox[0.5\textwidth]{}
		\end{subfigure}
	\end{figure}
\end{frame}
\begin{frame}
	\frametitle{Monte Carlo methods for thermal averages}
	\begin{block}{Thermal averages of a quantum system}
		\vspace{-4pt}
		\begin{equation}Z = \Tr[e^{-\beta H}]\quad,\quad \ev{A}_T=\frac1Z\Tr[Ae^{-\beta H}]\end{equation}
	\end{block}
	\begin{block}{Constructing the configurations}
		\vspace{-12pt}
	\begin{equation}
		\footnotesize
         \expval{A}_T = \frac1Z \sum_{n=0}^{\infty} \frac{(-\beta)^n}{n!}
                    \sum_{x_0,...,x_{n}}
                    \mel{x_0}{H}{x_1} ... \mel{x_{n-1}}{H}{x_n} \mel{x_n}{A}{x_0} 
	\end{equation}
	\begin{equation}
		w(x_0, ..., x_n) :=
			\frac{(-\beta)^n}{n!} \mel{x_0}{H}{x_1} ... \mel{x_{n-1}}{H}{x_n}
		\label{eq:weights_definition}
	\end{equation}
	%\begin{block}{}
		\begin{equation}
			\expval{A}_T = \frac1Z \sum_c w(c) A(c)
		\end{equation}
	\end{block}
\end{frame}
\begin{frame}
	\frametitle{The sign problem}
	\begin{equation}
		w(x_0, ..., x_n) :=
			\frac{(-\beta)^n}{n!} \mel{x_0}{H}{x_1} ... \mel{x_{n-1}}{H}{x_n}
	\end{equation}
	\begin{alertblock}{Achtung!}
		In some systems there exists $c$ such that $w(c)<0$.
		\begin{itemize}
			\item Typical scenarios for fermions
		\end{itemize}
	\end{alertblock}
	\begin{exampleblock}{Common workarounds and solutions}
		\begin{itemize}
			\item $A(c)\rightarrow-A(c)$: exponential errors $\frac{\Delta
				s}{\ev{s}} \propto e^{\beta N}$.
			\item Specific algorithms for some systems
		\end{itemize}
	\end{exampleblock}
	\begin{block}{In general NP-hard}
		A polynomial solution would imply P=NP
	\end{block}
\end{frame}

\section{QMS}
\begin{frame}
	\frametitle{A quantum computing solution}
	\begin{block}{Quantum Metropolis Sampling algorithm}
		\begin{itemize}
			\item Quantum computing algorithm
			\item Polynomially \textit{solves} some systems with the sign
				problem
			\item Requires efficient simulation of $e^{iHt}$
		\end{itemize}
	\end{block}
	\begin{block}{The idea}
		Evaluate $\ev{e^{-\beta H}}{\psi}$ without a Taylor series, 
		in the base of the hamiltonian eigenstates, avoiding the sign problem
	\end{block}
	\begin{block}{Sketch}
		\begin{itemize}
			\item Evaluate energy of a state and store in quantum
				register
			\item Minimize measurements to accept/reject decision
			\item Measure energy, the state collapse into an hamiltonian
				eigenstate: this is a validly sampled state.
		\end{itemize}
	\end{block}
\end{frame}
\begin{frame}
	\frametitle{Building blocks: QFT}
	
	\begin{block}{Setting}
		\begin{itemize}
			\item We have an $n$-qubit register in a generic, possibly non pure,
				state.
		\end{itemize}
	\end{block}
	\begin{equation}
		\ket{\psi_X} = \sum_Y \frac{\exp{2\pi i XY/2^n}}{\sqrt{2^n}} \ket{Y}
		\label{eq:fourier_basis}
	\end{equation}
	\begin{equation}
		U_{\text{QFT}} = \sum_X \ketbra{\psi_X}{X}
	\end{equation}
	\begin{block}{Important takeaway}
		\begin{itemize}
			\item It has an $\mathcal{O}(n^2)$ implementation
		\end{itemize}
	\end{block}
\end{frame}
\begin{frame}
	\frametitle{Building blocks: QPE}
	\begin{block}{Setting}
		\begin{itemize}
			\item An $n$-qubit unitary operator $U$, with an eigenstate
				$\ket{u}$ so that:
				$U\ket{u}=e^{i\theta}\ket{u}, \theta\in[0,2\pi)$
			\item A $n$-qubit ``system'' register $S$ initialized with $\ket{u}$
			\item An $r$-qubit ``energy'' register $E$ 
			\item Efficient implementation of c-$U^{2^j}$
		\end{itemize}
	\end{block}
	\begin{block}{Goal}
		\begin{itemize}
			\item Give the best $l$-qubit estimate for $\frac{\theta}{2\pi}$,
				$l\leq r$
		\end{itemize}
	\end{block}

\end{frame}
\begin{frame}
	\frametitle{Building blocks: QPE circuitry}
	\begin{figure}
		\centering
		\makebox[\textwidth]{
			\centering
			\Qcircuit @C=1.5em @R=.9em {
				\lstick{} & \multigate{1}{U^{2^2}} & \multigate{1}{U^{2^1}} &
					\multigate{1}{U^{2^0}} & \qw & \qw \\
				\lstick{} & \ghost{U^{2^0}} & \ghost{U^{2^1}} &
					\ghost{U^{2^2}} & \qw & \qw
				\inputgroupv{1}{2}{.8em}{1.0em}{\ket{u}_S}
					\\
				\lstick{\ket{+}_{E_1}} & \ctrl{-1} & \qw & \qw &
					\multigate{2}{\text{QFT}^\dag} & \qw\\
				\lstick{\ket{+}_{E_2}} & \qw & \ctrl{-2} & \qw & \ghost{\text{QFT}^\dag} & \qw\\
				\lstick{\ket{+}_{E_3}} & \qw & \qw & \ctrl{-3} & \ghost{\text{QFT}^\dag} & \qw
			}
		}
	\end{figure}
	\begin{block}{Important takeaway}
		Implementation complexity of:
		\vspace{-8pt}
		\begin{equation}
			\mathcal{O}(r^2)+\sum_{j=0}^{r-1}\text{ Complexity}(\text{c-}U^{2^j})
		\end{equation}
		\vspace{-8pt}
	\end{block}
\end{frame}
\begin{frame}
	\frametitle{QPE and ``Stone's unitaries''} 
	\begin{block}{A special case}
		\vspace{-8pt}
		\begin{equation}
			H=\sum_i \lambda_i\ketbra{\psi_i}{\psi_i}, \quad \lambda_i\in\mathbb R 
		\end{equation}
		\begin{equation}
			U= e^{iHt}
		\end{equation}
		\vspace{-8pt}
		\begin{itemize}
			\item QPE + measure on $E$ $\rightarrow$ measure $\frac{H}{2\pi}$
		\end{itemize}
	\end{block}
	\begin{block}{How to perform $U$: \textit{Trotter-Suzuki formulas}}
		\vspace{-16pt}
		\begin{align}
			e^{iHt} &= \left[e^{iH\delta}\right]^M = \left[e^{i\sum_jH_j\delta}\right]^M = \\ 
				&= \left[\prod_je^{iH_j\delta}+\mathcal{O}(\delta^2)\right]^M = \left[\prod_je^{iH_j\delta}\right]^M +\mathcal{O}(\delta)
		\end{align}
		\vspace{-8pt}
	\end{block}
\end{frame}
\begin{frame}
	\frametitle{QMS: step-by-step}
	\begin{block}{Setting}
		\begin{itemize}
			\item A ``system'' $n$-qubit quantum register $S$
			\item An \textit{efficient} $n$-qubit operator $e^{iHt}$,\\[0.2cm]
				\hspace{20pt}$H=\sum_jE_j\ketbra{\psi_j}{\psi_j}, \quad0\leq E_j<2\pi$
				\\[0.2cm]
			\item An ``energy'' $l$-qubit quantum register $E$
			\item An ``acceptance'' 1-qubit quantum register $a$
			\item \sout{Another ``old-energy'' $l$-qubit quantum register $E_\text{old}$}
				\begin{itemize}
			\item A classical $l$-bit register $E_\text{old}$
				\end{itemize}
		\end{itemize}
	\end{block}
	\begin{block}{Goal}
		\begin{itemize}
			\item Draw samples of the system state according to the Boltzmann
				distribution induced by the hamiltonian $H$.
		\end{itemize}
	\end{block}
\end{frame}
\begin{frame}
	\frametitle{QMS: step-by-step}
	\begin{figure}
		\centering
		\includegraphics[width=.5\textwidth]{images/QMSimages1.png}
	\end{figure}
	\begin{enumerate}
		\item[1.] Initial state:\\[0.2cm]
			%$\ket{\psi_0}_S \otimes \ket{0}_E \otimes \ket{0}_a ,
			%E_\text{old}=E_0$ \\[0.2cm]
			$\sum_j c_j \ket{u_j}_S \otimes \ket{0}_E \otimes \ket{0}_a ,
			E_\text{old}=E_0$ \\[0.4cm]
		\item[2.] QPE between $S$ and $E$:\\[0.2cm]
			$\sum_j c_j \ket{u_j}_S \otimes \ket{E_{j}}_E
			\otimes \ket{0}_a , E_\text{old}=E_0$\\[0.4cm]
		\item[3.] Measure and reset $E$, store the result in $E_\text{old}$:\\[0.2cm]
			$ \ket{u_j}_S \otimes \ket{0}_E \otimes \ket{0}_a,
							E_\text{old}=E_{j}$ \\[0.4cm]
	\end{enumerate}
\end{frame}
\begin{frame}
	\frametitle{QMS: step-by-step}
	\begin{figure}
		\centering
		\includegraphics[width=.6\textwidth]{images/QMSimages2.png}
	\end{figure}
	\begin{enumerate}
		\item[4.] An evolution $U$ is selected for the
			system register:\\[0.2cm]
			$\sum_k d_{kj} \ket{u_k}_S \otimes \ket{0}_E \otimes
			\ket{0}_a, E_\text{old}=E_{j}$\\[0.4cm]
		\item[5.] A new QPE is performed between $S$ and
			$E$: \\[0.2cm]
			$ \sum_k d_{kj} \ket{u_k}_S \otimes \ket{E_{k}}_E \otimes
			\ket{0}_a, E_\text{old}=E_{j}$ \\[0.4cm]
		\item[6.] Acceptance $a$ updated with accept-reject procedure: \\[0.2cm]
			\hspace{-20pt}
			$\sum_k d_{kj} \ket{u_k}_S \otimes \ket{E_{k}}_E \otimes
			\left(\sqrt{f(k,j)} \ket{1}_a+
			\sqrt{1-f(k,j)}\ket{0}_a\right)$\\[0.2cm]
			where: $f(k,j) = \min(1,e^{-\beta(E_k-E_j)})$\\[0.4cm]
	\end{enumerate}
\end{frame}
\begin{frame}
	\frametitle{QMS: step-by-step}
	\begin{exampleblock}{Case $a=1$: update accepted}
	\begin{enumerate}
		\item[7a.] Continue from step 1.
	\end{enumerate}
	\end{exampleblock}

	\vspace{10pt}
	\begin{alertblock}{Case $a=0$: update rejected}
	\begin{enumerate}
		\item[7b.] Revert to eigenstate of $H$ of energy $E_\text{old}$.
			\begin{enumerate}
				\item Apply the inverse of the operators we used in steps 4-6
					\\[0.1cm]
				\item Use QPE to measure the energy and check if
					$E=E_\text{old}$
					\\[0.1cm]
				\item If $E=E_\text{old}$, we have succeded. Else, repeat:
					\begin{itemize}
						\item reapply 4-6,
						\item measure a, 
						\item apply the inverse of 4-6,
					\end{itemize}
					until $E=E_\text{old}$.\\[0.1cm]
			\end{enumerate}
			Classical superpowers: at step 2 project $E$ into $E_\text{old}$
	\end{enumerate}
	\end{alertblock}
\end{frame}
\begin{frame}
	\frametitle{QMS: takeaways}
	Computationally intensive operations:
	\begin{itemize}
		\item QPE for $E_\text{new}$ and $E_\text{old}$
		\item Random matrix $U$
	\end{itemize}
	\begin{block}{Computational complexity of one step}
		\vspace{-8pt}
		\begin{equation}
			\text{Complexity}(\text{QPE}) + \text{Complexity}(U)
		\end{equation}
	\end{block}
	\begin{block}{When is QMS feasible?}
		\begin{itemize}
			\item When QPE is feasible, when hamiltonian simulations are
				feasible, most physical hamiltonians
		\end{itemize}
	\end{block}
\end{frame}
\begin{frame}
	\frametitle{QMS: sources of error}
	\begin{figure}
		\begin{subfigure}{0.48\textwidth}
			\includegraphics[width=\textwidth]{images/energycorr_vs_tau/main04-t.png}
			\caption{Rethermalization}
		\end{subfigure}
		\hfill
		\begin{subfigure}{0.48\textwidth}
			\includegraphics[width=\textwidth]{images/state_dispersion/E_3_polished.png}
			\caption{Digitalization of energy}
		\end{subfigure}
		\\[0.5cm]
		\begin{subfigure}{0.48\textwidth}
			\centering
			\includegraphics[width=.7\textwidth]{images/SquareLattice.png}
			\caption{Digitalization of states}
		\end{subfigure}
		\hfill
		\begin{subfigure}{0.48\textwidth}
			\centering
			\includegraphics[width=.5\textwidth]{images/trotter.png}
			\caption{Trotterization of evolution}
		\end{subfigure}
	\end{figure}
\end{frame}

\section{Hubbard}

\begin{frame}
	\frametitle{An useful testbench}
	Requests:
	\begin{enumerate}
		\item Reasonably simulable ($\sim$20 qubits)
		\item Physical interest
		\item Known results
		\item The QMS is a needed tool (sign problem)
	\end{enumerate}
	\vspace{20pt}
	The Hubbard model:
	\begin{enumerate}
		\item Smallest 2D model: 8+$l$+1 qubits
		\item Strongly correlated electron system
		\item Good amount of literature
		\item For some parameters, it has the sign problem
	\end{enumerate}
\end{frame}
\begin{frame}
	\frametitle{The Hubbard model}

	\begin{itemize}
		\item Spin-$\frac12$ fermions hopping on a graph $\Lambda$
		\item At most $N=2\left|\Lambda\right|$ fermions
	\end{itemize}
	\begin{figure}
		\centering
		\includegraphics[width=.7\textwidth]{images/HubbardModel/2Dimage-colour.png}
	\end{figure}
	\begin{equation}
		H = -J \sum_{\langle i,j \rangle,\sigma} \left(c_{i\sigma}^\dagger
		c_{j\sigma} + c_{j\sigma}^\dagger c_{i\sigma}\right)+ U \sum_{i} n_{i\uparrow} n_{i\downarrow}
		\label{eq:basic_hubbard}
	\end{equation}
\end{frame}
\begin{frame}
	\frametitle{Symmetries}
	\begin{block}{U(2) symmetry}
		\vspace{-10pt}
		\begin{equation}
			c_{i\sigma} \rightarrow U_{\sigma\sigma'} c_{i\sigma'}
		\end{equation}
		\vspace{-12pt}
		\begin{itemize}
			\item A global U(1) symmetry: particle conservation
			\item A global SU(2) spin symmetry
		\end{itemize}
	\end{block}
	\begin{block}{Particle-hole symmetry}
		\vspace{-10pt}
		\begin{equation}
			c_{i\sigma} \rightarrow \eta_i c_{i\sigma}^\dag \quad,\quad
		    c_{i\sigma}^\dag \rightarrow \eta_i c_{i\sigma}
		\end{equation}
		\vspace{-12pt}
		where $\eta_i = \pm 1$ on alternate sublattices.

		\vspace{4pt}
		\begin{equation}
			N \rightarrow 2|\Lambda| - N \quad,\quad
			H \rightarrow H + U (|\Lambda| - N)
		\end{equation}
	\end{block}
\end{frame}
\begin{frame}
	\frametitle{Theoretical insights: Lieb's theorem}
	\centering
		$H = -J \sum_{\langle i,j \rangle,\sigma} \left(c_{i\sigma}^\dagger
		c_{j\sigma} +\text{h.c.}\right)+ U \sum_{i} n_{i\uparrow}
		n_{i\downarrow}$
		\vspace{10pt}
	
	\begin{minipage}[c]{0.55\textwidth}
	\begin{block}{Hypothesis}
		\begin{itemize}
			\item Hubbard model with $U > 0$
			\item Graph $\Lambda$ bipartite in $A$ \& $B$
			\item $N=|\Lambda|$ even
		\end{itemize}
	\end{block}
		\vspace{10pt}
	\end{minipage}
	\hfill
	\begin{minipage}[c]{0.4\textwidth}
	\begin{figure}
		\centering
		\includegraphics[width=.8\textwidth]{images/Bipartite/Bipartite-colour.png}
		\caption{A bipartite graph}
	\end{figure}
	\end{minipage}
	\begin{block}{Thesis}
		\begin{itemize}
			\item The ground state has total spin $|S| = \frac12 \big||A| - |B|\big|$ 
			\item Its degeneracy is only the $2S+1$ spin degeneracy
		\end{itemize}
	\end{block}
\end{frame}
\begin{frame}
	\frametitle{Theoretical insights: Nagaoka's theorem}
	\centering
		$H = -J \sum_{\langle i,j \rangle,\sigma} \left(c_{i\sigma}^\dagger
		c_{j\sigma} +\text{h.c.}\right)+ U \sum_{i} n_{i\uparrow}
		n_{i\downarrow}$
		\vspace{10pt}
	\begin{block}{Hypothesis}
		\begin{itemize}
			\item $U=\infty$
			\item $J\geq0$
			\item $N=|\Lambda| \pm 1$
		\end{itemize}
	\end{block}
	\begin{block}{Thesis}
		\begin{itemize}
			\item The ground state has total spin $|S| = \frac12 (|\Lambda|-1)$ 
			\item Its degeneracy is only the $2S+1$ spin degeneracy
		\end{itemize}
	\end{block}
	\begin{itemize}
		\item Schumann's analytical solution of 2x2 Hubbard model:
			$U_\text{Nag}=4J(2+\sqrt7)\sim 19J$
	\end{itemize}
\end{frame}
\begin{frame}
	\frametitle{Speculative phase diagram at $T=0$}
	\begin{figure}
		\centering
		\includegraphics[width=.7\textwidth]{images/Arovas_2022_PD_x.png}
		\hspace{20pt}
		\caption{Speculative phase diagram of the Hubbard model at $T=0$, as a
		function of $U/J$ and $x=(N-|\Lambda|)/|\Lambda|$}
	\end{figure}
	%Probably many slides to describe the various regions. Same graph, different
	%captions in each slide. And maybe something to highlight on the graph the
	%region we are talking about. And maybe some references to actual
	%simulations.
\end{frame}
\begin{frame}
	\frametitle{Introducing the chemical potential}
	\begin{equation}
		\small
		H = -J \sum_{\langle i,j \rangle,\sigma} \left(c_{i\sigma}^\dagger
		c_{j\sigma} +h.c.\right)+ U \sum_{i} n_{i\uparrow} n_{i\downarrow}
		- \mu \sum_{i,\sigma} n_{i,\sigma}
		\label{eq:chemical_hubbard}
	\end{equation}
	\begin{itemize}
		\item Still U(2) global symmetry
		\item Particle-hole symmetry: $\mu\rightarrow-\mu$
	\end{itemize}
	\begin{figure}
		\centering
		\begin{subfigure}{.43\textwidth}
			\centering
			\includegraphics[width=\textwidth]{images/Arovas_2022_PD_x.png}
			\caption{}
			\label{fig:arovas_2022_pd_x}
			\vspace{7pt}
		\end{subfigure}
		\begin{subfigure}{.41\textwidth}
			\centering
			\includegraphics[width=\textwidth]{images/Arovas_2022_PD_mu.png}
			\caption{}
			\label{fig:arovas_2022_pd_mu}
		\end{subfigure}
	\end{figure}
\end{frame}

\section{Errors}
\begin{frame}
	\frametitle{Errors: what matters and what doesn't}
		\begin{alertblock}{Yields relevant errors}
			\begin{itemize}
				\item Rethermalization
				\item Digitalization of energy
				\item Classically simulating $e^{iHt}$
			\end{itemize}
		\end{alertblock}
		\begin{exampleblock}{Yields zero or negligible errors}
			\begin{itemize}
				\item Digitalization of the system's state
				\item Trotterization of $e^{iHt}$
			\end{itemize}
		\end{exampleblock}
\end{frame}
\begin{frame}
	\frametitle{Errors: rethermalization times}
	\begin{itemize}
		\item Undesired measurements correlation
		\item Disruption of the system's state if $[A, H]\neq0$
	\end{itemize}
	\begin{figure}
		\centering
		\includegraphics[width=0.9\textwidth]{images/energycorr_vs_tau/main04-t.png}
	\end{figure}
\end{frame}
\begin{frame}
	\frametitle{Rethermalization: undesired correlations}
	\begin{figure}
		\centering
		\begin{subfigure}{\textwidth}
			\centering
			\includegraphics[width=0.8\textwidth]{images/csn_vs_time/thermo1.png}
			\caption{Rethermalization time: 1 QMS step}
		\end{subfigure}
		\vspace{6pt}

		\begin{subfigure}{\textwidth}
			\centering
			\includegraphics[width=0.8\textwidth]{images/csn_vs_time/thermo15.png}
			\caption{Rethermalization time: 15 QMS step}
		\end{subfigure}
	\end{figure}
\end{frame}
\begin{frame}
	\frametitle{Errors: digitalization of energy}
	\begin{itemize}
		\item Inaccurate representation of energy
		\item Unwanted state transitions
	\end{itemize}
	\begin{figure}
		\begin{subfigure}{0.7\textwidth}
			\centering
			\includegraphics[width=\textwidth]{images/muj_ls/muj_l4/js_rich_00/son.png}
			\caption{$l=4$ qubits for the energy register}
		\end{subfigure}
		\begin{subfigure}{0.7\textwidth}
			\centering
			\includegraphics[width=\textwidth]{images/muj_ls/muj_l6/js_rich_00/son.png}
			\caption{$l=6$ qubits for the energy register}
		\end{subfigure}
	\end{figure}
\end{frame}
\begin{frame}
	\frametitle{Errors: digitalization of energy}
	\begin{figure}
		\begin{subfigure}{0.48\textwidth}
			\centering
			\includegraphics[width=\textwidth]{images/state_dispersion/E_3_polished.png}
		\end{subfigure}
		\hfill
		\begin{subfigure}{0.48\textwidth}
			\centering
			\includegraphics[width=\textwidth]{images/state_dispersion/E_4_polished.png}
		\end{subfigure}
		\begin{subfigure}{0.48\textwidth}
			\centering
			\includegraphics[width=\textwidth]{images/state_dispersion/E_5_polished.png}
		\end{subfigure}
		\hfill
		\begin{subfigure}{0.48\textwidth}
			\centering
			\includegraphics[width=\textwidth]{images/state_dispersion/E_6_polished.png}
		\end{subfigure}
		\caption{Non-interacting 2x2 lattice, with chemical potential $\mu =
		1$. The ground state has zero energy. Depicted is the first excited
		state for different number of ``energy'' qubits}
	\end{figure}
\end{frame}
\begin{frame}
	\frametitle{Errors: digitalization of energy}
	\begin{figure}
		\centering
		\includegraphics[width=\textwidth]{images/energy_vs_time/main04.png}
		\caption{$ g(\min(\{E_j\}))\rightarrow0, \;
		g(\max(\{E_j\}))\rightarrow2^l-1,  \;l=6$}
		\label{fig:reth_en_time_a0}
	\end{figure}
\end{frame}
\begin{frame}
	\frametitle{Errors: digitalization of energy}
	\begin{figure}
		\centering
		\includegraphics[width=\textwidth]{images/energy_vs_time/main05.png}
		\caption{$ g(\min(\{E_j\}))\rightarrow a,
		g(\max(\{E_j\}))\rightarrow2^l-1-a, l=6,  a=11$}
		\label{fig:reth_en_time_a10}
	\end{figure}
\end{frame}
\begin{frame}
	\frametitle{Errors: classically simulating $e^{iHt}$}
	No need for real trotterization in a classical simulation
	\vspace{10pt}

	Still, how to perform $e^{iHt}$?
	\begin{table}
		\begin{tabular}{r|p{0.7\textwidth}}
			$\mathcal O (2^{3n})$ & Dense matrix algebra  \\[0.2cm]
			$\mathcal O (2^{2n})$ & Taylor expansion + sparse matrix algebra \\[0.2cm]
			$\mathcal O (2^{n})$ & Taylor expansion + sparse ``vector'' algebra \\
		\end{tabular}
	\end{table}
	Sparse ``vector'' algebra:
	\begin{itemize}
		\item divide $t$ into $M$ small steps $\delta$ so that
			$\delta \lVert H\rVert_\text{max}\sim1$
		\item Use the Krylov Arnoldi method $M$ times to apply the exponential of a
			sparse matrix $e^{iH\delta}$ to the system's state vector
	\end{itemize}
\end{frame}
\section{Results}
\begin{frame}
	\frametitle{Simulation params and observables measured}
	\begin{table}
		\centering
		\begin{tabular}{r|m{0.3\textwidth}}
			lattice size & 2x2 \\[0.3cm]
			energy qubits & 6 \\[-0.2cm]
			QMS steps & 
				{\begin{align*} & \\[-1cm]
					10000 \quad &@\beta=50 \\[-0.1cm]
					4000 \quad &\text{otherwise}
				\end{align*}}
				\vspace{-20pt}
		\end{tabular}
		\caption{Main simulation parameters}
	\end{table}
	\begin{block}{Observables measured}
		\begin{itemize}
			\item square of per-site z-spin: $\langle n_\uparrow - n_\downarrow\rangle^2$
			\item average double occupancy: $\langle n_\uparrow n_\downarrow\rangle$
			\item electron density: $\langle n_\uparrow + n_\downarrow - 1
				\rangle =(N-|\Lambda|)/|\Lambda|$
		\end{itemize}
	\end{block}
\end{frame}
\begin{frame}
	\frametitle{At half-filling}
	\begin{figure}
		\centering
		\makebox[\textwidth]{
			\includegraphics[width=1.1\textwidth]{images/muj_scan_10s/muj_scan_10/js_rich_00/son.png}
		}
	\end{figure}
\end{frame}
\begin{frame}
	\frametitle{Away from half filling half-filling}
	\begin{figure}
		\centering
		\vspace{-8pt}
		\makebox[\textwidth]{
			\includegraphics[width=0.8\textwidth]{images/muj_scan_10s/muj_scan_10/js_rich_03/son.png}
		}
		\vspace{-18pt}

		\makebox[\textwidth]{
			\includegraphics[width=0.8\textwidth]{images/muj_scan_10s/muj_scan_10/js_rich_04/son.png}
		}
	\end{figure}
\end{frame}
\begin{frame}
	\frametitle{Trading reporting accuracy for intuitive clarity 1}
	\begin{figure}
		\centering
		\makebox[\textwidth]{
			\includegraphics[width=1.1\textwidth]{images/muj_scan_02/eriksen_csn.png}
		}
	\end{figure}
	\begin{itemize}
		\item Build a colormap for each observable in the $\mu-J$ plane
		\item Combine the colormaps in an RGB image, each observable to a different channel
	\end{itemize}
\end{frame}
\begin{frame}
	\frametitle{Trading reporting accuracy for intuitive clarity 2}
	\begin{figure}
		\centering
		\makebox[\textwidth]{
			\includegraphics[width=1.1\textwidth]{images/muj_scan_10s/muj_scan_10/eriksen_csn.png}
		}
	\end{figure}
	\begin{figure}
		\centering
		\includegraphics[width=0.4\textwidth]{images/Arovas_2022_PD_mu.png}
	\end{figure}
\end{frame}
\begin{frame}
	\frametitle{At finite temperatures: changes}
	\begin{figure}
		\centering
		\vspace{-8pt}
		\includegraphics[width=.99\textwidth]{images/muj_scan_10s/muj_scan_10/eriksen_csn.png}
		\includegraphics[width=.99\textwidth]{images/muj_scan_10s/muj_scan_11/eriksen_csn.png}
	\end{figure}
\end{frame}
\begin{frame}
	\frametitle{At finite temperatures}
	\begin{figure}
		\centering
		\vspace{-8pt}
		\includegraphics[width=.99\textwidth]{images/muj_scan_10s/muj_scan_11/eriksen_csn.png}
		\includegraphics[width=.99\textwidth]{images/muj_scan_10s/muj_scan_12/eriksen_csn.png}
	\end{figure}
\end{frame}
\begin{frame}
	\frametitle{At finite temperatures}
	\begin{figure}
		\centering
		\vspace{-8pt}
		\includegraphics[width=.99\textwidth]{images/muj_scan_10s/muj_scan_13/eriksen_csn.png}
		\includegraphics[width=.99\textwidth]{images/muj_scan_10s/muj_scan_14/eriksen_csn.png}
	\end{figure}
\end{frame}
\section{Conclusion}
\begin{frame}
	\frametitle{Conclusion}
	Summing up:
	\begin{enumerate}
		\item Thermal averages of small scale 2D Hubbard model
		\item Good qualitative match of the ground state, \\but for the antiferromagnetic phase
	\end{enumerate}
	\vspace{10pt}

	Possible follow-ups:
	\begin{enumerate}
		\item More resources:
			\begin{itemize}
				\item larger lattices
				\item more qubits in the energy register
			\end{itemize}
		\item Exploiting the scaling function $H\rightarrow H_\text{eff}$
		\item Use quantum floating-point arithmetic
		\item Other (quantum) algorithms like VQE
		\item Combine with ECC and run on a quantum computer
			\vspace{-10pt}
			\begin{multicols}{2}
			\begin{itemize}
				\item Needed: \\$\sim10^4$/QMS step
				\item Now: \\$\sim10^3$ gates
			\end{itemize}
			\end{multicols}
			\vspace{-10pt}
	\end{enumerate}
\end{frame}
\section*{}

\begin{frame}[noframenumbering]
	\begin{center}
		\large \textbf{Main references} \normalsize
	\end{center}
\makebox[\textwidth]{
	\color{white}
	\cite{Arovas_2022}
	\cite{Temme_2011}
	\cite{Lloyd_1996}
	\cite{Troyer_2005}
	%\cite{Bogna_2020}
	\cite{Hatano_2005}
	\cite{Schumann_2001}
	%\cite{Arora_2007}
	%\cite{Higham_2008}
	\cite{Bravyi_2002}
	%\cite{Whitfield_2016}
	\cite{Clemente_2020}
}
	\vspace{-26pt}
	\printbibliography
\end{frame}

\begin{frame}[noframenumbering]
	\begin{center}
		\Large
		Thanks for listening
	\end{center}
\end{frame}

\section*{Extras}
\begin{frame}
	\frametitle{QMS: the random $U$ matrix}
	\begin{block}{How to choose $U$}
		\begin{itemize}
			\item Ergodicity inside $\mathcal H$
			\item $U$ and $U^\dag$ have same probability $\rightarrow$ detailed
				balance
		\end{itemize}
	\end{block}
	\begin{block}{A weaker condition for ergodicity}
		\begin{itemize}
			\item \textit{Almost any} $U$ ensures ergodicity between different
				eigenspaces of $H$
			\item We can change $U$ only each time we measure an observable
				other than $H$
		\end{itemize}
	\end{block}

\end{frame}
\begin{frame}
	\frametitle{Theoretical insights: Hubbard square}
	Hubbard square: 2x2 2D model
	\begin{block}{Solved}
		\begin{itemize}
			\item Numerically solvable: Hilbert dims $2^{2\times2\times2}=256$ 
			\item Analytically solved by Schumann in 2001
		\end{itemize}
	\end{block}
	\begin{table}
		\footnotesize
		\begin{tabular}{c|c|c|c|c}
			N & S & range of $U$ & $E_0$ for $U\ll J$ & $E_0$ for $U\gg J$\\
			\hline \hline
			0 & 0 & any $U$ & 0 & 0 \\
			\hline
			1 & 1/2 & any $U$ & $-2J$ & $-2J$ \\
			\hline
			2 & 0 & any $U$ & $-4J+\mathcal{O}\left(U/J\right)$ &
			$-2\sqrt2J+\mathcal O ((\frac JU)^2)$ \\
			\hline
			\multirow{2}{*}{3} & 1/2 & $U<U_\text{Nag}$ &
			$-4J+\mathcal{O}\left(U/J\right)$ & - \\
			\cline{2-5}
			 & 3/2 & $U>U_\text{Nag}$ & - & $-2J$ \\
			\hline
			4 & 0 & any $U$ & $-4J+\mathcal{O}\left(U/J\right)$ & $-12J^2/U$ \\
		\end{tabular}
		\caption{Ground state properties, where $U_\text{Nag}=4J(2+\sqrt{7})\sim
		19J$}
	\end{table}
\end{frame}
\begin{frame}
	\frametitle{At half-filling the model is sign problem free}
	\begin{itemize}
		\item Special case of $N = | \Lambda |$: 
			\begin{itemize}
				\item algorithm from 80s by \textit{White et al.}
			\end{itemize}
	\end{itemize}
	\begin{figure}
		\centering
		\includegraphics[width=.7\textwidth]{images/loh_1990_6b.png}
	\end{figure}
\end{frame}

\begin{frame}
	\frametitle{Rethermalization: system's state disruption}
	\begin{figure}
		\centering
		\begin{subfigure}{\textwidth}
			\centering
			\includegraphics[width=0.8\textwidth]{images/UW_thermo/muj_scan_UW_thermo_1/js_rich_00/son.png}
			\caption{Rethermalization time: 1 QMS step}
		\end{subfigure}
		\vspace{6pt}

		\begin{subfigure}{\textwidth}
			\centering
			\includegraphics[width=0.8\textwidth]{images/muj_ls/muj_l6/js_rich_00/son.png}
			\caption{Rethermalization time: 15 QMS step}
		\end{subfigure}
	\end{figure}
\end{frame}

\begin{frame}
	\frametitle{Actual quantum computer performances}
	\begin{block}{Quantum volume (IBM)}
		\begin{equation}
			\log_2V_Q = \text{argmax}_n\left[min(n, d(n))\right]
		\end{equation}
	\end{block}
	\begin{itemize}
		\item 2022 IBM: $\log_2 V_Q = 9$
		\item 2023 Quantinuum (previously Honeywell): $\log_2 V_Q = 15$
	\end{itemize}
	\vspace{20pt}
	So how many gates before losing too much information?\\
	With $\log_2 V_Q = 15$:
	\begin{itemize}
		\item 225 gates for a square circuit 15x15
		\item possibly more for different circuits
	\end{itemize}
\end{frame}

\end{document}	
